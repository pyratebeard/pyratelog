Most Linux users are aware of or have probably used `top` for monitoring system resources and processes.  A lot of people these days have also heard of `htop`, which is like `top` but with some extra features.

I would like to recommend another tool, `atop`, which is like `top` but more advanced.  The `atop` tool shows you system resources, processes, and even network usage, but the reason I like it so much is the logging and replay capability.

As a sysadmin there are many times I have needed to set up process and resource logging for troubleshooting issues and this tool has been able to provide me some much needed data.

You can install it from the repos on Arch, {Debian,Ubuntu}, {Fedora,RHEL,CentOS}
```
pacman -S atop
apt install atop
dnf install atop
yum install atop
```

You can also download it from the [website](https://www.atoptool.nl/){target="_blank" rel="noreferrer"}.

Once installed running it is as easy as
```
atop
```

There is _a lot_ of information displayed. The top half of the screen shows system resources while the lower half gives you information about the processes.

Full details of the information provided can be read in the manual page.

Start the `atop` service to begin logging (use appropriate alternative command for none systemd systems)
```
systemctl start atop
```

The logs are written to /var/log/atop by default, with the date suffix.  To read the raw format logs incant
```
atop -r /var/log/atop/atop_$(date +%Y%m%d)
```

This may not look any different but take note of the timestamp at the very top, it should be the time logging started.

Use `t` and `T` to go forward and back in time intervals, type `?` for a list of keybindings.

By default the time interval is 10 minutes, this can be changed by editing the LOGINTERVAL variable in /etc/sysconfig/atop.  To reduce the interval to one minute (60 seconds) incant
```
sed -i 's/^LOGINTERVAL=600/LOGINTERVAL=60/' /etc/sysconfig/atop
```

Another useful feature `atop` has is system activity reports, similar to the `sar` command.

The reports can viewed with the command `atopsar` and there are reports for CPU, memory, load average, disk, and much more.  To view all reports incant
```
atopsar -A
```

Information on the report options can be found in the `atopsar` manual page.

I have found `atop` very useful over the years, being able to step through what the system was doing minute by minute as an issue occurred is really valuable.  Even more so when you don't have to be there at the time (these issues always seem to occur in the middle of the night).
