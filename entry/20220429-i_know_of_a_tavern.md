I have had a Mastodon account since 2017 on [mastodon.social](https://mastodon.social/){target="_blank" rel="noreferrer"}.  As with other social networks my posting is sporadic at best, although in recent months I have tried to be more active.

Hosting your own instance of Mastodon is one of the features I was drawn to, self-hosting anything is great in my eyes.  That being said, I had never actually sat down and set up my own instance... until now! *dum dum dummm*.

At first I tried to be clever (read "make things stupidly complicated for no good reason") and thought I could host my new Mastodon instance in an LXC container behind HAProxy [like I do the rest](20220127-multi_lxc_with_haproxy.html){target="_blank" rel="noreferrer"}.  Installing Mastodon was easy, really easy.  I hit some trouble with the SSL configuration though.  It can probably be done but my spare time to work on projects like this is short these days so I abandoned ship and went the easy, boring route.

My new instance is running on a dedicated small VPS (1 vCPU, 2GB RAM).

Before installing Mastodon I needed to have a mail server so I installed [Postfix](https://www.postfix.org/){target="_blank" rel="noreferrer"}.
```
apt install mailutils
apt install postfix
```

After running through the `postfix` configuration tool I also made two small changes to /etc/postfix/main.cf.  First I changed the `inet_interfaces` to loopback
```
inet_interfaces = loopback-only
```

Then I added my domain to `mydestination`
```
mydestination = $myhostname, localhost.$mydomain, $mydomain
```

Now I could begin the install of Mastodon.  As I said, it is really easy.  I followed the [offical guide](https://docs.joinmastodon.org/admin/install/){target="_blank" rel="noreferrer"} and had it done in about 20 minutes.

Once my new instance was running I used the account forwarder tool built into Mastodon to redirect people to my new account.  The [advised way](https://mastodon.social/@Gargron/103393780267601137){target="_blank" rel="noreferrer"} of migrating to a new instance is to sign up on the new server, then in the settings select "Moving From another account".  This sets up an alias for the account.

At this point I hit the only issue in the whole process.  When I tried to create the alias I got the "Something went wrong" error page.  After a quick search of the error in the logs I found a [pull request](https://github.com/mastodon/mastodon/pull/18004/commits/866451337e2f43e99ade9c8dfacac588ec3833ca){target="_blank" rel="noreferrer"} on the Mastodon repo which fixed the issue.  The change was only a few lines of Ruby so I made the change on my system and restarted the mastodon-web service.

Creating the alias worked and on my old mastodon.social account I could use the "Moving To another account" option to migrate my followers.  Unfortunately this does not migrate any of my past toots but the old account should remain visible, albeit greyed out.

The whole process was really straightforward, once I stopped digging the LXC shaped hole.  I haven't had a chance to dig into all the administration tools or federating with other instances yet, this will be looked at over coming weeks.

For now though, if you want to follow my new account you can find me [@pyratebeard@harbour.cafe](https://harbour.cafe/@pyratebeard){target="_blank" rel="noreferrer"}.  If you are already following me the Mastodon magic should take care of the change.
