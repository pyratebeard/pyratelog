After learning about [registers](20230511-reg_5_standing_by.html){target="_blank" rel="noreferrer"} in Vim, [macros](https://vim.fandom.com/wiki/Macros){target="_blank" rel="noreferrer"} were a logical next step.

Macros use registers as well but instead of just saving chunks of text they record repeatable steps meaning the same actions can be performed again and again.

To start recording a macro, in Normal mode hit `q` and a register, i.e `qa`

The status line will now show that a recording is taking place
```
recording @a
```

Any actions you take now will be recorded to register `a`.  To stop a recording hit `q` again.  To replay the macro use `@` followed by the register, `@a`.  If you want to play the last played macro hit `@@`.

Sometimes you may want to edit a macro.  If it is a lot of steps you won't want to record the whole thing again so it is possible to edit it in Vim's command line.

1. declare the register you are setting with `:let @a='`
2. type Ctrl-r Ctrl-r `a` to paste in the contents of register `a`
3. edit as required
4. append an apostrophe, `'`, to close the statement and hit Return

If you need to add a control character within the macro, such as Return, type Ctrl-v followed by the required key.  For example _Ctrl-v Return_ produces `^M`.

To see the Vim help for this method incant
```
:help i_ctrl-v
```

As macros are saved in registers viewing them is the same as viewing registers
```
:registers
```

It is also possible to declare macros in ~/.vimrc so that they are available to each session.  This is done using the same `let` declaration shown above.

In ~/.vimrc
```
let @t='<macro>'
```

For every Vim session opened the `t` register will always be set to this and can be played using `@t`.

If you want to save a recorded macro to ~/.vimrc, in Insert mode use _Ctrl-r Ctrl-r t_ to paste in the contents, save having to type it all out again.

Getting the hang of macros can be fun as well as frustrating.  Spending longer creating a perfect macro than the time taken to complete the task is probably quite common, but when you get it the satisfaction is "*chefs kiss*".

To see the Vim help page on macros incant
```
:help recording
```
