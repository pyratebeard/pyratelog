I am a command line enthusiast so a lot of the utilities I use are terminal based, including my password manager of choice, [pass](https://www.passwordstore.org/){target="_blank" rel="noreferrer"}.

My email client of choice is also terminal based, [mutt](http://www.mutt.org/){target="_blank" rel="noreferrer"}.  I track the muttrc config file as part of my [dotfiles](https://gitlab.com/pyratebeard/dotfiles){target="_blank" rel="noreferrer"} repo and obviously I don't want to keep passwords in this file.

For a long time I would have the password in the file on my local system and had to remember to be careful when adding and committing changes to my repo.  All that remembering was exhausting.

About a year ago I finally started using [offlineimap](https://github.com/OfflineIMAP/offlineimap){target="_blank" rel="noreferrer"} to sync my mail to my local system.  While setting up my offlineimaprc config I found that it is possible to import passwords using python.  I created a simple python file
```
#!/usr/bin/env python
from subprocess import check_output

def get_pass(account):
    return check_output("pass email/" + account + "/passwd", shell=True).splitlines()[0]
```

then imported the file in the `[general]` section of offlineimaprc, and imported the password using `remotepasseval` in the repository section
```
[general]
pythonfile = ~/.offlineimap.py

[Repository remote]
remotepasseval = get_pass("pyratebeard")
```

I use the `account` argument as I have multiple accounts with different passwords.

While setting this up I also discovered that commands can be used in muttrc to import paswords

```
set my_pass = "`pass email/pyratebeard/passwd`"

set smtp_pass = $my_pass
```

Now I can forget about all that remembering.
