## random.movie.recommendation
One Point O aka Paranoid 1.0 ([TMDB](https://www.themoviedb.org/movie/17110-one-point-o)) is a sci-fi mystery with horror elements and a dystopian cyberpunk feel to it.

#### synopsis
> Paranoid computer programmer Simon wakes up to find a package in his room one day. Despite attempts at securing his apartment, the packages keep arriving. While cameras watch Simon's every move, he struggles to find the answers to the mysterious forces taking over his life.

---

Lead by Jeremy Sisto (Simon) the cast all play their roles brilliantly, including excellent performances from Lance Henriksen and cult icon Udo Kier.

The movie does a great job of conveying Simon's paranoia and distrust of others. The truth is revealed at a pace which keeps you interested while you enjoy the peculiar goings on in and around the apartment block.

I love this cyberpunk style, much more than the bright, shiny, neon aesthetic it seems everybody is leaning into these days. The rundown buildings, hacked together tech, the baneful products of corporations doing whatever it takes to keep you hooked, backdropped with dark atmospheric and electronic music.

If you are a fan of dystopian cyberpunk mystery with some obsessive paranoia and a creepy droid head, you should check this movie out.
