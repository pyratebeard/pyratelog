After a [previous post](20220512-distrust,_but_verify.html) contemplating losing access to my phone and therefore to all my servers while on the move, I have been working on a solution.

Parts of this little project I had been meaning to do for a while, my previous post was a catalyst to get it all sorted.

The first thing I did was build an SSH bastion, or "jump server", a small server that I will use to proxy all my SSH connections to my other servers.  This is easily done with SSH, add the following to ~/.ssh/config
```
Host *
	ProxyJump bastion
```

In order to secure all my other servers I restrict SSH connections to only be allowed from my bastion, in `iptables` this can be achieved by incanting
```
sudo iptables -A INPUT -p tcp -s <ip_address>/<subnet> --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
```

Along with some further hardening this setup works well; yet what if, as mentioned in the previous post, I don't have access to one of my devices?  I looked into hardware tools like [Yubikey](https://www.yubico.com/){target="_blank" rel="noreferrer"}, so far I haven't decided which to go with.  Instead, for the time being at least, I have put a single, passphrase protected,, SSH private key on a small flash drive I can put on my keys.  This particular flash drive also has a USB-C connection so can be used on a lot of modern mobile devices.  In principle this means all I need to access my bastion is my flash drive and a device capable of SSH.

The flash drive key only grants access to the bastion, I didn't want to allow connections through to my other servers.  On the bastion I have another passphrase protected key pair which can access my other servers.  Even if my first key is compromised and used before I can remove the key from the bastion, a second key will be required to get any further.

As mentioned in the previous post I also have a VPN running on my home network, allowing me to remote in to my home network on my phone and laptop when away.  In the event I don't have access to either I have put a certificate on my bastion so I can remote home.

Accessing my home VPN relies on knowing the IP address my ISP assigns me.  On rare occasions this IP changes, and it has caught me out in the past.  For a long time I have been using a script on a Pi at home which routinely checks the external IP, then sends a [notification](20220104-notify,_only_smaller.html) and an email if it changes.  To check the external IP incant
```
dig +short myip.opendns.com @resolver1.opendns.com
```

Up to now the script worked well but I felt like the setup could be improved.  My updated setup now has a domain pointed at my home network configured to use Dynamic DNS.  I run [ddclient](https://github.com/ddclient/ddclient){target="_blank" rel="noreferrer"} on my Pi at home so if the IP ever changes the domain DNS _should_ be updated.  I now have all my VPN certs pointing to the domain name.

Without using my VPN I can also clone my `pass` git repository directly onto the bastion, giving me access to my passwords and backup codes in an emergency.

There are still more improvements I can make to this new setup.  I am still looking at hardware authentication tools so that will most likely be the next step.  In the event I lose my flash drive, and my phone, and my laptop, I have been thinking of putting a fallback ssh key on my Mega storage account.  I don't use my Mega account for anything important so disabling MFA wouldn't be so bad (it actually has to be disabled for use with [rclone](https://rclone.org/){target="_blank" rel="noreferrer"}).  Meaning all I need to access my bastion is a web browser to download the key, then SSH.  That should be achievable from anywhere.

It may also be worth adding a secondary bastion in the event my current one disappears or is compromised and needs to be destroyed.  Another bastion wouldn't be too difficult, it merely costs money.  I may also add Dynamic DNS to my bastion in case of an IP change.  It is unlikely to happen, nevertheless I want to cover all scenarios.

The next big step is to test this solution.  I know the SSH access works as required, and I know my flash drive key works, so now I need to go out into the wild to test from a completely random system.  Testing the Dynamic DNS for my VPN will need to be done sooner rather than later, I don't want to get caught out again.

So far this solution has given me a little relief knowing that I should be able to access my servers, my passwords and backup codes, and even my home network if I am stranded without my phone or laptop.
