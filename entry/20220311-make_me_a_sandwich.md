#### TL;DR
Alias to allow aliases with `sudo`
```
alias sudo='sudo '
```

---

I am a big fan of using command aliases on my Linux systems.  If you are too you may have also been hit by the `command not found` error when attempting to run `sudo <alias>`.

As an example I have an alias for `ls`, which is based on the `ll` alias common on Red Hat systems
```
alias ll='ls -lahF --color=auto'
```

If you try to run as sudo it won't work
```
sudo ll
sudo: ll: command not found
```

We can change that by adding another alias
```
alias sudo='sudo '
```

Now we can run our aliases as sudo.  This also works for `unbuffer`, which as I mentioned in [a previous post](20220303-unbuff_colours.html) doesn't recognise aliases.
```
alias unbuffer='unbuffer '
unbuffer ap playbook.yml | tee results.out
```

Do you have any fun alias hacks?
