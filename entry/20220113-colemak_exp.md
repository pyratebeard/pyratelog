We all know that QWERTY is the de-facto standard when it comes to keyboard layouts, at least in the English speaking world.  The reason behind why this layout was used is unclear, but what ever the reason it seemed to stick around.

This is not the only keyboard layout.  You may have heard of, or even used, layouts such as QWERTZ and AZERTY, both of which are popular in Europe.

There is also the Dvorak layout developed by Dr. August Dvorak.  The Dvorak layout is suppose to be faster and more comfortable than QWERTY.  It is very different, so understandably many find it difficult to learn.  The symbol keys have shifted a lot in Dvorak, although this apparently makes it good for programming.

<img src="/img/dvorakkb.png" class="fitwidth" alt="dvorak layout" />

_source: https://dvorak-keyboard.com/_

I have never tried QWERTZ or AZERTY before.  On a virtual keyboard on my phone I tried Dvorak but couldn't get the hang of it.  So I stuck with QWERTY, and lived happily ever after.

Until a few days ago when somebody mentioned Colemak

```
 % pyrate │ you ever have one of those days when you just can't type properly?
...
 @   dcat │ pyrate: not since i switched to colemak
...
 % pyrate │ dcat: interesting. i've never tried colemak
...
 @   dcat │ pyrate: you should try it
 @   dcat │ jolia: it's a much more logical layout
...
 % pyrate │ dcat: i will give it a shot
```

That weekend I thought, frak it!  Why not?  If nothing else it gives me something to write about.  I rearranged the keys on my keyboard and switched the keymap to Colemak.  The hurdles started immediately.

In order to understand the hurdles let me go through how I use QWERTY.

## backtick the future
In the terminal I use [tmux](https://tmux.github.io) a lot.  The default prefix keybinding is Ctrl+b, but this is one too many keys and I don't find it comfortable.  I remap the prefix to backtick, which is quicker.

If you want to use this add the following to the top of your ~/.tmux.conf
```
# change prefix to backtick
unbind C-b
set-option -g prefix `
bind ` send-prefix
```

This worked well until I started using a 68 key keyboard and the backtick was the same key as Esc but on different layers.  To switch layers I have to type Fn+q.  As a Vim user I _need_ Esc!  How can I use both without switching layers?

## escape from capslock
I am not a loud person so I don't use Capslock much, if at all.  For a long time I have remapped Capslock to Ctrl as I found it easier than using the left Ctrl key.  To set this for a US keyboard incant
```
setxkbmap -option ctrl:nocaps us
```

Change `us` to `gb` for a British keyboard.

According to the word on the internet there are some people who remap Capslock to Esc.  I thought about this but didn't want to have to give up Ctrl.

Enter [xcape](https://github.com/alols/xcape).  This tool allows your to bind a modifier key to two actions, one when pressed and released, and a second when held down.  As I have Ctrl in place of Capslock I can use `xcape` so that when I press and release Ctrl it issues Esc and remains as Ctrl when held down.  To do this, install the `xcape` package then incant
```
xcape -e 'Control_L=Escape'
```

## flight of the navigation
I use the text editor Vim.  A lot.  Cursor navigation in Vim famously uses H, J, K, L on the 'home row' (left, down, up, right respectively).  A lot of new users find this confusing but once you get use to it you are hooked, at least I was.  I got so hooked that I started using the same navigation everywhere I could.  If it wasn't already available in the tool I would remap the keybindings.  On servers I would `set -o vi` to change the command line editing mode.  It is everywhere.

## one flew over the colemak nest

<img src="/img/Colemak_layout_2.png" class="fitwidth" alt="colemak layout" />

_source: https://colemak.com_

The standard Colemak layout switches Capslock to Backspace.  This first hurdle broke my remapping of Capslock to Ctrl/Esc.  After a bit of digging I found that you can create a custom variant of the keymap to change Backspace to Control_L (left Ctrl).

As root or with `sudo` add the following to /usr/share/X11/xkb/symbols/us
```
// colemak-ctrl
partial alphanumeric_keys
xkb_symbols "colemak-ctrl" {
    // include standard colemak layout
    include "us(colemak)"

    key <CAPS> { [ Control_L, Control_L, Control_L, Control_L ] };
};
```

You also need to add the following to /usr/share/X11/xkb/rules/base.xml
```
<variant>
  <configItem>
    <name>colemak-ctrl</name>
    <description>English (Colemak-ctrl)</description>
  </configItem>
</variant>
```

and add this line to /usr/share/X11/xkb/rules/base.lst under the `!variant` section (search for the other Colemak line)
```
colemak-ctrl    us: English (Colemak-ctrl)
```

Finally, restart Xorg so the changes are picked up.  To set the new Colemak variant and `xcape` incant
```
setxkbmap -option ctrl:nocaps us -variant colemak-ctrl
xcape -e 'Control_L=Escape'
```

You would think that because we have set Backspace to Control_L we wouldn't require the `-option ctrl:nocaps`, but when I tested it Ctrl wasn't issued when I held the key yet Esc was when I pressed and released.  Including the option gave me the desired action and I didn't want to open another can of worms just yet.

The second hurdle was using Vim, or more specifically using Vim-style navigation.  As you can see from the image above the H, J, K, L keys are not in a nice neat row.  This threw me straight away.  I didn't want to start remapping everything, and in Vim the N, E, and I keys are used as other bindings which would have to be remapped... where does it stop?!

My keyboard doesn't have customisable layers, a solution that a number of people use.  I looked around for a software fix and found I could use a combination of `xbindkeys`, which I use already, and `xte`.

The `xte` package generates fake input, so if you incant
```
xte 'key j'
```

`xte` will press and release the J key.  Combine that with `xbindkeys` and I can sort of get N and E working as J and K by pressing Alt+N or Alt+E.  For this you need to add the following to your ~/.xbindkeysrc
```
## vi navigation
"/usr/bin/xte 'key j'"
  release+alt + n

"/usr/bin/xte 'key k'"
  release+alt + e
```

This works in Vim and works in a few other tools but not everything.  I think by using Alt some tools get confused.  From the conversations I have seen online it is advised to break your muscle memory completely and learn to use H, J, K, L keys in their new configuration.  I have started to try that but will be keeping `xte` around for a bit just in case.

The third hurdle is the one I think everybody hits, muscle memory.  I found that I have to look at the keyboard a lot more.  If I look away I start to type in QWERTY, aks ar jyl ekyw ghag cak bf vfpj cyktlrukd.

I have been using Colemak for a few days now.  I went all in, so even while working I am using it.  The first couple of days were tough.  Frustrating more than anything as I am so accustomed to my typing speed.  It is already getting easier though, I can feel new muscle memory developing and my speed increasing each day.

So, I have given myself a month to use the new layout and I shall report back.

(This post was typed using Colemak... and it was slow.)
