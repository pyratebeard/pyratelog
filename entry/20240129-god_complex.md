I am a hypocrite.

As a seasoned sysadmin I get paid to go into clients and complain about how poorly they manage system security.  Okay, that's not really my job, but it's what I seem to spend a lot of time doing.

One of my main vexations is incorrect, or more likely non-existent, configuration of `sudo`.  I am constantly trying to stop the use of `ALL=(ALL) ALL` and instead make use of the powerful control the _sudoers_ config provides (or stop people logging in when they have no reason to).

I, on the other hand, am GOD!  So I switch to the root user to do most, if not all of my work when on a remote system.  I am a hypocrite.

I do try to use `sudo` as I preach, and if configured correctly it works.  Unfortunately most places I work are still not there yet, so in order to do get any work done, root I must become.

On my own systems none of this is a problem.  I have my user account, which has full root access, and I switch to the root user if needed.

That is until I stumbled across an article by Dmitry Khlebnikov, [Should we use "sudo" for day-to-day activities?][1].

Dmitry argues that the use of `sudo` is less of a requirement on a correctly secured and managed system, adding that it may in fact introduce security flaws and bad practices.

After reading it I thought about this article for a while.  Then I thought I would try something on my personal systems.

I agree with Dmitry that logging in with the username root, even via `ssh`, is still not a good idea.  Instead I created a new user with the user ID (UID) of 0 and group ID (GID) of 0
```
useradd -o -u 0 -g 0 -m -d /home/enoch enoch
```
* the `-o` option allows the creation of an account with an already existing UID

On Linux and Unix systems there will always be a UID of 0, and the assigned username is generally "_root_".  The convention of naming that user root was probably taken from [Multics][2], and stems from the naming of the `/` or _root_ directory.  The actual username can be anything.  I toyed with the idea of changing the `/etc/passwd` entry from root to something else, but didn't know what that may break down the line as there may be some software expecting a username of root. Probably best to leave it alone.

Adding a second user with UID 0 isn't unheard of.  FreeBSD ships with the user _toor_ (root backwards) intended as a backup or sometimes to use an alternative shell.  An interesting post on [daemonforums.org][3] gives a nice explanation.

As my new user has the UID of 0 it is effectively root
```
pyratebeard@kinakuta:~$ sudo -u enoch -i
root@kinakuta:~# whoami
root
```

I updated my SSH server config to allow root login without a password, this still allows root login with ssh keys
```
PermitRootLogin prohibit-password
```

I also updated my `~/.ssh/config` accordingly to use the new username on my remote systems.  Now when I `ssh` to any of them I am logging in directly as root.

You may be thinking that this has made my systems insecure, but I don't feel it has.  My use of SSH keys, and recently [SSH Certificate Authorities][4], puts me in a good place for login security.  For local systems in my house there is no external access accept via VPN.  For remote systems I make use of a [bastion][5], with firewall rules on all my remote systems to only allow connections to port 22 (SSH) from my bastion.
```
iptables -A INPUT -p tcp -s <bastion_ip>/<cidr> --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -j DROP
```

This change has altered how I work on my own systems without the need to escalate privileges, more importantly however, it has given me new ideas of how to provide effective and efficient security to my clients (if any of them can get over years of the "no root" mindset).

[1]: https://dmitry.khlebnikov.net/2015/07/18/should-we-use-sudo-for-day-to-day-activities/
[2]: https://multicians.org/
[3]: https://daemonforums.org/showthread.php?t=666
[4]: 20240102-respect_my_authoritah.html
[5]: 20220830-a_well-fortified_position.html
