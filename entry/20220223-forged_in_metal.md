Whenever anybody asks me what music I listen to my answer is always the same, heavy metal.  This is not an all-encompassing answer, like a lot of people I don't just listen to one type of music or even one genre of metal.

Still, metal is definitely _my_ music.  This wide ranging collection of genres has given the world some of the greatest musicians to have lived, and me some of my closest friends.  While there is, of course, different opinions on the best bands, best songs, or best genres, (most of) the fans are some of the friendliest people I have met.  From picking you up in the mosh pit to giving you a comradely nod in the street.  We all have one thing binding us, our fierce love of heavy metal.

I didn't start my journey through rock and metal until my teenage years.  The following is a breakdown of the albums which have been pivotal in shaping my tastes.  Each one having such an influence it forever altered my love and appreciation of music and to some degree formed who I am today.  Here is the list for those eager to know:

* Aerosmith - _Get A Grip_
* Ozzy Osbourne - _The Ozzman Cometh_
* Black Label Society - _1919 Eternal_
* Ensiferum - _Iron_

These aren't necessarily my favourite albums of all time, or even my favourite album by the artists, but the point in time I heard each of them proceeded to change everything.

## out of the shadows
I grew up mostly listening to the music of my parents such as Buddy Holly, Cliff Richard, and my Dad's favourite, The Shadows. He liked them so much I was almost named either Hank or Marvin.  Luckily my Mum had a say in the decision.

This sort of music was fun growing up and my Dad and I learnt a lot of The Shadows songs on the guitar.  As I grew into my teens and out of my parents' music I didn't really develop my own taste in music, just listening to the radio and whatever was popular at the time.

Everything changed in 1999 at the age of 14, with a holiday to Disney World Orlando and a ride on the Rock 'n' Roller Coaster.

## another dimension
### aerosmith - _get a grip_
The Rock 'n' Roller Coaster featured the song _"Walk This Way"_ by Aerosmith, and I was immediately blown away.  From the opening guitar riff to the unusual vocals I had never heard music that made me feel... something.  I didn't know what I was feeling, who Aerosmith were, or even what type of music this was, yet I wanted more.

Not long after the holiday to Orlando I went to visit one of my older sisters and mentioned this great song I had heard.  Amazingly my sister had heard of Aerosmith and had some of their CDs in her collection.  She handed me a CD with a cow's pierced udder on the cover, and told me to check it out.

On the way home I was so excited, checking out the bizarre album cover with no idea what I was about to hear.  As soon as I got home I put the CD on and couldn't stop listening to it.  My senses were overwhelmed with amazing guitar riffs, weird lyrics, and the whole theatre of the band.  A whole new world of music had just been opened up to me.  I was slow to look around though, and I don't remember listening to anything other than Aerosmith for about a year.

I recognise now I have an obsessive trait, at the time all I knew was I enjoyed this band and wanted to know everything about them.  I learnt the band members names, all their albums and the year of release, I started reading music and guitar magazines mentioning them, and read their biography.  I had never been a "fan" of a band before but I just knew I was an Aerosmith fan.

## into darkness
### ozzy osbourne - _the ozzman cometh_
Over the next year I did eventually start listening to something other than Aerosmith.  A music bond had started with my sister and this grew to include her husband who introduced me to the music of Joe Satriani, Dokken, Pink Floyd, Uriah Heep, and much more.  He would become a big influence on my music tastes over the next few years.

One day I was digging through my brother-in-law's CDs when I found a black cover showing a gold cross with the word OZZY embossed on it.  The cover, although simple, immediately grabbed my attention.  I turned the case over to see a black and white picture of a long haired man in chain mail sitting on a stool.

Who the hell was this man, and what's an Ozzy?  My brother-in-law put the album on for me and we sat and listened to the whole thing.  The picture of Ozzy on the back of the album is forever carved into my memory.  I don't know what it is about the image yet I was drawn to it.  Ozzy's unique voice and his crazy, bat eating, larger than life personality was fascinating to me.  All the other bands and artists I listened to were great, yet there was something about Ozzy and his music.  My journey was advancing from rock to metal.  I was on the edge of a world of heavy distortion, fantastical lyrics, and dark imagery; it was awe-inspiring.

For the next couple of years I listened to so much music.  I devoured Ozzy's back catalogue.  My brother-in-law let me listen to his CDs, everything from Alice Cooper to Tygers of Pan Tang.  I would watch Kerrang TV, rocking out to all the new metal at the time.  I would read rock and metal magazines and consume all the music on the cover CDs.  I started to grow my hair and wear a leather jacket.  My brother-in-law also took me to concerts in the nearby cities, seeing bands live cultivated my love of this music even further.

## merciless forever
### black label society - _1919 eternal_
One day in 2002 I was at my sisters when my brother-in-law came home with a new CD.

He told me Ozzy's guitarist Zakk Wylde had a solo project called Black Label Society, and it was heavier than the music he played with Ozzy.

I still get chills listening to the opening riff of the first song, _"Bleed For Me"_.  This heavy, brutal album was the best thing I had heard in years.  Zakk was this huge, bearded figure wearing his bands logo on a leather vest, playing a unique looking "bullseye" guitar. Everything about Zakk and this band was what 16 year old me wanted.  From the pinched harmonics to the biker-esque fan club of "Society Dwellers".  I wore my Black Label colours with pride, I scrawled the BLS logo and "SDMF" all over my school and college books.  Not even my obsessions with Aerosmith or Ozzy reached the peak of my love for Black Label Society.

---

Over the years I had made friends who had the same love of metal.  We would spend hours together listening to or talking about music.  We would go to gigs and recommend bands and albums back and forth.

At the end of 2003 I joined the Air Force and didn't really find anyone as interested in metal as I was.  There were a few friends who would chat about what was current, but it wasn't the same as the friends I had made back home.  Luckily a couple of the friends from home stayed in touch and whenever I got a weekend to go back we would hang out listening to metal again.  When one of my friends moved in with his brother I was introduced to the next person who would have a profound impact on me and my metal journey.

## raised horns
### ensiferum - _iron_
Very early on in our friendship I was introduced to the song _"Lai Lai Hei"_ by Ensiferum.  I was suddenly thrown in to a whole new genre of metal, with folk instruments.  I got a copy of the _Iron_ album and fell in love with this amazing blend of styles.  The folk metal genre enlightened me to epically long songs, obscure traditional instruments, and ignited my interest in pagan cultures.

Ensiferum were at the same time heavier and more melodic than the metal I had been a fan of.  Other folk metal bands such as Finntroll and Turisas played songs that you could not just headbang to, but actually dance around to.  I will never forget seeing thousands of metalheads polkaing to Korpiklaani's _"Let's Drink"_ at Bloodstock Open Air 2007.

My friend became an invaluable resource for me, introducing me to new bands every time we met.  I used to love going to his house and looking through his immense CD collection, and his even more impressive digital collection.  I started expanding my love of metal into various genres such as power, death, black, and all the atmospheric, melodic, and symphonic genres that branch out and cross over.

---

I am still discovering new music every week, and my friend is still providing me with excellent recommendations (check out his weekly radio show [Powerzone](https://powerzonemetal.uk){target="_blank" rel="noreferrer"} if you want some recommendations as well).

It was always clear in my mind which albums where the turning points along my path.  Since _Iron_ I have listened to some amazing albums, both old and new, and while bands and albums have refined my likes and dislikes none have had such an impact on me than those I have written about here.  What type of music I listen to on any given day can change depending on my mood yet whenever anybody asks me what music I listen to my answer is always the same.

## honourable mentions
While writing this post I listened back through a lot of the music I had spent time with over the years.  The nostalgia was extremely enjoyable, remembering all the people, gigs, and forgotten songs that have made me who I am.  There have been so many excellent albums I would never be able to list them all, however I think these albums deserve a special mention for their part along my journey:

* Alice Cooper - _Hey Stoopid_
* Iron Maiden - _Brave New World_
* Rob Zombie - _The Sinister Urge_
* Battleheart (now Alestorm) - _Battleheart_
* Svartsot - _Ravnenes Saga_
* Insomnium - _Winter's Gate_

