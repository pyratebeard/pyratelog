Have you ever needed to quickly send a file, directory, or even some text from one computer to another?  Maybe you don't have SSH set up, or any way of using `rsync`/`scp`.  If you are talking to a friend and want to quickly send them a file there are many ways of doing it but I think Magic Wormhole is one of the quickest (after setup).  Once installed you can issue a send command and be presented with an "short and human-pronounceable" code which you use with the receive command on the other computer.

The code can be found on [Github](https://github.com/warner/magic-wormhole) and the documentation on [Read the Docs](https://magic-wormhole.readthedocs.io/en/latest/). 

Magic Wormhole may be available in your package manager or you can install from `pip`.  These are the steps I took to setup.

First I created a python virtual environment then installed the `magic-wormhole` package
```
python3 -m virtualenv /path/to/wormhole_venv
source /path/to/wormhole_venv/bin/activate
(wormhole_venv) pip install magic-wormhole
(wormhole_venv) deactivate
```

To make my life a little easier I created a link to the `wormhole` command in a directory in my $PATH
```
ln -s /path/to/wormhole_venv/bin/wormhole ~/bin/wormhole
```

Now I can send a file to a friend
```
wormhole send funny.jpg

Sending 254.7 kB file named 'funny.jpg'
Wormhole code is: 2-hemisphere-ruffled
On the other computer, please run:

wormhole receive 2-hemisphere-ruffled
```

Wormhole waits for the transfer to take place.  On my friend's computer they can receive the file
```
wormhole receive 2-hemisphere-ruffled
Receiving file (254.7 kB) into: funny.jpg
ok? (Y/n): Y
Receiving (->tcp:127.0.0.1:33985)..
100%|████████████████████████████████████████| 255k/255k [00:00<00:00, 1.77MB/s]
Received file written to funny.jpg
```

My send is now complete
```
Sending (<-127.0.0.1:41324)..
100%|████████████████████████████████████████| 255k/255k [00:00<00:00, 54.1MB/s]
File sent.. waiting for confirmation
Confirmation received. Transfer complete.
```

This is a one time only code and the wormhole is only open for the duration of the transfer.

You can also send arbitrary text
```
wormhole send --text "hello friend"
```

The great thing about this tool is that you don't need any prior credentials or connection to the system like you would with SSH.  It is also possible to run your own [Mailbox server](https://github.com/warner/magic-wormhole-mailbox-server/blob/master/docs/welcome.md) if you so desire.

Thanks to Brian Warner ([twitter](https://twitter.com/lotharrr)/[github](https://github.com/warner)) for Magic Wormhole.
