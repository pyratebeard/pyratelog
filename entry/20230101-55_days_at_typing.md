It is a year to the day [since I started](20220101-absolute_git.html){target="_blank" rel="noreferrer"} the [100DaysToOffload](https://100daystooffload.com/){target="_blank" rel="noreferrer"} challenge.

The idea of the challenge is to publish one hundred blog posts in a year.  For those that have been keeping track I managed just over half the requirement, publishing entry [number fifty-five](20221226-tty_twister.html){target="_blank" rel="noreferrer"} on the 26th December.

From the very start I knew that publishing one hundred entries would be a lot of work but I was motivated and prepared to give it a shot.  I made a list of as many ideas as I could and with my creative juices flowing started writing everyday.  Most of my writing has been on my phone, a lot of the early ones were written while holding my sleeping infant daughter.

Let us take a look at the distribution of entries over the last year
```
ls -1 entry/2022* | cut -c 11-12 | uniq -c
     12 01
     12 02
      5 03
      4 04
      3 05
      1 06
      1 07
      3 08
      1 09
      4 10
      5 11
      4 12
```

As you can see I started off the year strong.  Twenty four entries in the first two months.  That is almost half my final number published already.

March was a bad month.  I caught Covid-19 and was ill for about two weeks.  For a couple of months after I also felt exhausted all the time, although having a one year old to look after did not help.  Due to this I stopped writing every day, which in turn stalled my motivation and even my creativity.

The summer was a blur of work and parenting, barely getting a single entry published for a few of the months.  Around this time I started to develop some rules on my writing.  I had been drafting posts when I thought of what to write, but if a draft remained untouched for a few weeks, or I couldn't find the words, then it was forgotten.  A number of the ideas I had on my list were either started as a draft or ignored if I couldn't think of how to expand on the premise.

My entries are also quite short on average.  I have been told by some readers that they like this.  Even my most lengthy entries can be read in 10 to 15 minutes.  I try to develop my drafts in one or two sittings.  If I can write a whole entry in half an hour then it will most likely get published.

Using that mindset has helped me to pick up the publishing a bit more near the end of the year to a comfortable rate.

A lot of the shorter entries were written at the beginning of the year but I quickly used up that backlog attempting to stick to publishing every two to three days.  By the end of the year I was writing as and when the ideas came along.  Either something new I was working on, or I had fleshed out an idea enough to start writing.

Considering between 2016 and 2021 I had only managed sixteen entries, I would say fifty-five in 2022 was a success.  It has certainly got me into the habit of writing more and not only trying to write about technology.  My [forged in metal](20220223-forged_in_metal.html){target="_blank" rel="noreferrer"} entry was one of the most enjoyable to write.

Looking forward I won't be attempting the 100DaysToOffload challenge again (for now) but will continue writing when I can.  A few entries a month seems to be a sustainable rate for me at the moment.

There are no analytics on this site (by design) so I have no idea how many people visit and read, but if you are one of them then Thank You.  I hope you have enjoyed some of my ramblings.

As always, if you would like to get in touch my contact details can be found on my [home page](https://pyratebeard.net){target="_blank" rel="noreferrer"}.  Comments and suggestions are always welcome.
