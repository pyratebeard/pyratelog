Over the last year I have started listening to podcasts on a more regular basis.  Most of the time I have about 20 to 30 minutes while I am doing a chore or some other task.  For a long time I was listening to [Cory Doctorow's reading](https://craphound.com/hackercrackdown.xml) of [The Hacker Crackdown](https://www.gutenberg.org/ebooks/101) by Bruce Sterling, but felt like it was taking me too long to get through the episodes 20 minutes at a time.

I decided to start speeding up the podcast so I could get through more.  First I sped up 1.10x to see what it was like but very quickly moved to 1.25x.  I got so accustomed to listening at 1.25x that when I put an episode on a different device I thought Cory was talking really slow.

As I grew use to the speed I started listening to all my podcasts at 1.25x.  After a while I increased to 1.30x, then 1.40x.  Now I can listen to almost all at 1.50x speed.  This has really changed my podcast listening and I don't feel like I am falling behind so much.

On occasion the speed has to be dropped down again if somebody is talking quite fast at normal speed but I am surprised at how quickly I have become use to it.

I have started trying this for videos as well.  Some vlogs I subscribe to can be sped up to about 1.35x at the moment.  I think visual at speed is harder than audio.  I have even managed to watch a few movies at 1.25x.

Now I need to work on improving my speed reading...
