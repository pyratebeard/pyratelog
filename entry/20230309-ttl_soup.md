After switching to using [gpg for my ssh key](20221202-secret_agent_man.html){target="_blank" rel="noreferrer"} I have found one particular issue that started to bug me.

As part of my workflow when I open a terminal and my gpg passphrase TTL has expired I will be prompted to re-enter it.  This works well, and I have set the default cache TTL to 28800 seconds which is about 8 hours, roughly a work day.

When I started using gpg-agent for ssh I noticed that this setting was not observed for the ssh key.  After a brief investigation I discovered that you can add a custom value after the keygrip in ~/.gnupg/sshcontrol, as noted in the `gpg-agent` man page.

> _An entry starts with optional whitespace, followed by the keygrip of the key given as 40 hex digits, optionally followed by the caching TTL in seconds and another optional field for arbitrary flags.  A non-zero TTL overrides the global default as set by --default-cache-ttl-ssh._

In my ~/.gnupg/sshcontrol file I put `28800` after the keygrip and restarted the agent.  This did not appear to have the desired effect.

This past week I found the time to do a bit more digging.  First I checked the ssh TTL options that `gpg-agent` had picked up, to do this incant
```
gpgconf --list-options gpg-agent | grep cache-ttl-ssh
```

I was looking to see if the default settings had been overridden
```
default-cache-ttl-ssh:24:1:expire SSH keys after N seconds:3:3:N:1800::
max-cache-ttl-ssh:24:2:set maximum SSH key lifetime to N seconds:3:3:N:7200::
```

Looking at the output I can see there is no value in the last field so the default has not been overridden.

By adding the following options to ~/.gnupg/gpg-agent.conf I was able to set the value
```
default-cache-ttl-ssh 28800
max-cache-ttl-ssh 28800
```

After restarting `gpg-agent` I checked the options and this time it looked good
```
 ──── ─ gpgconf --list-options gpg-agent | grep cache-ttl-ssh                                                                                              ~/.default-cache-ttl-ssh:24:1:expire SSH keys after N seconds:3:3:N:1800::28800
default-cache-ttl-ssh:24:1:expire SSH keys after N seconds:3:3:N:1800::28800
max-cache-ttl-ssh:24:2:set maximum SSH key lifetime to N seconds:3:3:N:7200::28800
```

This means that my ssh key passphrase is cached for the duration of the work day.

Now, before some of you start shouting that it is not a good idea to leave the cached credentials for that long, do not fear.  I have a script that runs when I lock my screen which kills gpg-agent.  This means that if I leave my desk the cached passphrases are dropped.  When I return and either open a new terminal or run a command which uses my gpg or ssh keys I will be prompted to re-enter the passphrase.  I had decided to set the TTL to 8 hours so that while I am working for long continuous periods I don't need to worry about having to re-enter my passphrase, I am pretty lazy after all.

Interestingly I found a [closed bug report](https://dev.gnupg.org/T1053){target="_blank" rel="noreferrer"} on the GnuPG bug tracker, which mentions this exact issue, "_The TTL specified in sshcontrol for SSH keys is ignored_".  The report was closed in 2009 so I am surprised I am experiencing the issue.  I will dig a bit deeper to see if I have missed something but if anybody knows anything about this please let me know (contact information can be found on my [homepage](https://pyratebeard.net){target="_blank" rel="noreferrer"}).
