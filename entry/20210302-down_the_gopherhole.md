In the early Nineties, before HTTP and the World Wide Web really took off, a team at the University of Minnesota wanted a better way to distribute and search for documents over the Internet.  They developed a protocol called Gopher.

This protocol, which was text based and accessed via terminals, was very popular for a number of years but ultimately lost out to HTTP and the graphical web browsers that we now know.

Up until recently I had never used Gopher, and thought it had been lost long before my time.  A discussion on the [Nixers](https://nixers.net/Thread-Gopher-anyone?pid=21384#pid21384) forum led to a Nixers community Gopher hole, gopher://g.nixers.net.

To access Gopher you need a browser or client that supports it.  I personally use [`sacc`](git://bitreich.org/sacc.git), you can use the `lynx` terminal browser available on most Linux distributions, or [Pocket Gopher](https://f-droid.org/packages/com.gmail.afonsotrepa.pocketgopher/) on Android.

## burrowing in

Let's say you installed `sacc`.  Once installed open the Nixers link
```
sacc gopher://g.nixers.net
```

and you should be presented with something like this
```
    | 
    | NIXERS.NET community burrow
    | 
Text+ About this server
Dir + Server News
Text+ FAQ
    | 
Text+ Who is online?
Text+ Yggdrasil peers
    | 
    | Our lovely members' holes:
    | 
Dir + ~anon
Dir + ~ekangmonyet
Dir + ~evbo
Dir + ~eyenx
Dir + ~jolia
Dir + ~mcol
Dir + ~opFez
Dir + ~pyratebeard
Dir + ~ramiferous
Dir + ~vnm
Dir + ~z3bra
```

You can use `j` and `k` to navigate up and down by a single line, or `J` and `K` to jump to next/previous link respectively.

If you scroll down to the "About this server" line you will see it says "Text" on the left hand side (in `lynx` it says "File").  This tells you what sort of link it is.  The most common are "Text" or "File", "Dir", and "HTML".

Gopher is designed with a hierarchical structure so it is easy to navigate.  Text files will open in a pager, in my case `less`.  Directories will open a new Gopher index page.  HTML links should open in your web browser.  You may also find Binary files which can be downloaded to your local machine.

When you are on a link use `l` to open it.  If you open a new directory you can use `h` to go back.

Have a look around the Nixers Gopher site, check out our members' holes.  You will find some phlogs or rlogs which are Gopher blogs.  You may also find links to other Gopher servers.

If you navigate to my Gopherhole, either through the Nixers link or directly at gopher://g.nixers.net/1/~pyratebeard, you will find a link to a guide on Gopher hosted at FloodGap, and Gopherpedia, which is a Gopher interface to Wikipedia.  There is also a huge Gopher community at [SDF](https://phlogosphere.org/).

It is exciting to see this cool protocol still being used after 30 years and hopefully you will check it out and dig your own Gopher hole.
