In 2009 while on deployment I picked up a battered old sci-fi novel from the shelf of books in our equipment tent.

I remember something about a spaceship crashing on an alien world and the crew having to travel through a dangerous jungle.  That is it.  I don't remember the name of the novel or the author.  I don't remember the characters, major plot points, or even what the cover looked like.

In the back of my mind I thought I know what the cover was but could never quite bring that image forward.

For years I have searched for this novel.  It is very hard to search for "spaceship crash in alien jungle", there is a surprisingly large number of sci-fi novels with spaceships and jungles.

Sometimes I think I have come close but have never stumbled upon "the one".  That was until last week.  As happens on occasion I was going through another bout of searching when I happened on one of the cover images for "The Legion of Space" by Jack Williamson ([ISFDB](http://www.isfdb.org/cgi-bin/pl.cgi?222345){target="_blank" rel="noreferrer"}).  The cover caught my eye and so I read the plot summary on [Wikipedia](https://en.wikipedia.org/wiki/The_Legion_of_Space#Plot_summary){target="_blank" rel="noreferrer"} and while most of it didn't sound familiar there was one paragraph;

> Through the machinations of his uncle, a powerful politician with a hidden agenda, John Ulnar is assigned to Aladoree's guard force at a secret fort on Mars. When she is kidnapped by a huge alien spaceship, John and the three other survivors of the guard force follow her kidnappers to a planet of Barnard's Star. They crash-land and must battle their way across a savage continent to the sole remaining citadel of the Medusae.

I decided to give it a read, hoping for a moment of remembrance.  First I had a look on [Project Gutenberg](https://www.gutenberg.org/ebooks/bookshelf/68){target="_blank" rel="noreferrer"} to no avail.  They have some "Astonishing Stories", which Legion of Space was serialised in before being published, but I didn't know the issues and didn't really want to go digging through them all.

I checked my local library but they also did not carry a copy.  Thus I succumbed to Amazon.  The novel was available on Kindle so I purchased it, excited that this may finally be it.

My first thoughts were that my search had failed again.  Nothing was sparking any memories.  The plot, the characters, I didn't remember a thing.

Soon the writing, Williamson's descriptive prose, started to feel familiar.  Yet still no sudden recollection.  Then the spaceship crash on a alien planet and the crew having to venture into a deadly jungle.  The only plot points I vaguely remember, and yet, I couldn't be confident this was the same book.

After finishing the novel a few days ago I have tried to accept that The Legion of Space is probably the novel I read back in 2009.  I also am having to accept that for some reason I cannot, and probably never will, actually remember.  There are lots of memories I have from that deployment, almost all of it (I think), but not the name, or even the cover art, of that novel.

Hopefully by finding The Legion of Space I can finally lay this quest to rest.  At the very least I have read a(nother) decent sci-fi adventure novel about a spaceship crash in an alien jungle.
