It has now been [8 months](20220113-colemak_exp.html) since I started using the Colemak keyboard layout.  After [a few initial issues](20220214-colemak_lvl_up.html) things are going well.

Vim navigation is no longer a problem, I am comfortable with the new positions of h, j, k, and l.  In the last month or so I have also acquired a second keyboard, so I have one permanently configured with QWERTY and can quickly switch them around.  This has made dealing with VMs much easier.

## patch all the things
A new issue that arose was on updating the xorg packages.  The update replaced /usr/share/X11/xkb files, thus removing my colemak-ctrl custom config.

After a bit of digging I found that I could put my colemak-ctrl setting into its own file under /usr/share/X11/xkb/symbols/, instead of the 'us' file.  I also found that I don't necessarily need the entries in /usr/share/X11/xkb/rules/base.{lst,xml}.  For reference /usr/share/X11/xkb/symbols/colemak-ctrl looks like this
```
// colemak-ctrl
partial alphanumeric_keys
xkb_symbols "colemak-ctrl" {
    // include standard colemak layout
    include "us(colemak)"

    key <CAPS> { [ Control_L, Control_L, Control_L, Control_L ] };
};
```

Now instead of selecting the variant
```
setxkbmap -option ctrl:nocaps us -variant colemak-ctrl
```

I can incant
```
setxkbmap -option ctrl:nocaps colemak-ctrl
```

## these boots are made for typing
The keymap on boot issue was also resolved by adding the following to /etc/vconsole.conf
```
KEYMAP=colemak-ctrl
```

Then I included the keymap hook before the encrypt hook in /etc/mkinitcpio.conf
```
HOOKS=(base udev autodetect modconf block keymap encrypt lvm2 filesystems keyboard fsck)
```

I also set the keymap for X11 by adding the following to /etc/X11/xorg.conf.g/20-keyboard.conf
```
Section "InputClass"
    Identifier "keyboard"
    MatchIsKeyboard "yes"
    Option "XkbLayout" "colemak-ctrl"
EndSection
```

## neverending story
I can confidently say I am now a Colemak user.  In the beginning I thought this little experiment wouldn't last and by now I would have returned to QWERTY, but quite the opposite in fact.  I feel at home using Colemak as my daily driver, and even looked into replacing the keyboard on my laptop with Colemak.

Hopefully my journey will inspire somebody else to give it a go.
