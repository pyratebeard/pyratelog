Earlier today I saw [this article](https://lwn.net/SubscriberLink/880458/5c4147ec8a7ca8df/) on LWN.net (via lobste.rs) about a new feature in OpenSSH 8.9, [SSH agent restriction](https://www.openssh.com/agent-restrict.html), that will allow you to restrict the hosts your keys can be used on with `ssh-agent`

As a daily user of SSH (Secure SHell) I am a big fan of using SSH keys over passwords.  Keys are more secure than passwords and by using `ssh-agent` you can enter your passphrase once, leaving you free to `ssh` all over the place without constantly typing your passwords.

This new restriction feature looks to quite useful if you're using agent forwarding through a bastion host.  You will be able to specify which hosts the keys will authenticate with and even through which hops.  The links above have some great examples and much more information into how it works.

A number of people in the comments of that article pointed out that you can use the SSH config option `ProxyJump` or `-J` on the commandline.  With agent-forwarding through a bastion a user would normally `ssh` to the bastion and then `ssh` again to the desired host.  This creates two encrypted connections.  Using `ProxyJump`, however, works by getting the bastion to forward the first encrypted connection without breaking it.  From your terminal you can incant
```
ssh -J user@bastion user@host
```
This will open a connection with "host" in one go.  Especially useful if you don't trust the bastion.

I use `ProxyJump` quite a lot.  Hopping through a bastion is common practice when you have isolated networks. I also use it with my LXC containers, by jumping through the host server into the container.  To achieve that my `~/.ssh/config` looks something like this
```
Host mainframe
  HostName 67.13.5.100
  Port 2222

Host lxc1
  HostName 10.0.3.24
  ProxyJump mainframe

Host lxc2
  HostName 10.0.3.50
  ProxyJump mainframe
```

With this config I can `ssh` straight to my containers with one encrypted connection through the host.
```
ssh lxc1
```

I will still be interested in the agent restriction feature when it comes in, and if you know of any other useful SSH tricks I would love to hear them.
