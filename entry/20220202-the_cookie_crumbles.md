I am [not a fan](https://log.pyratebeard.net/entry/20220108-type_cookie_you_idiot.html) of the abusive use of cookies and the despicable consent pop-ups.  So I am delighted to read that a decision has been reached by the Belgian Data Protection Authority that online advertising trade body IAB Europe's consent pop-ups are unlawful, and they must delete all user data for violating GDPR.

You can read more about this on the [Irish Council for Civil Liberties](https://www.iccl.ie/news/gdpr-enforcer-rules-that-iab-europes-consent-popups-are-unlawful/) website, and here is the [PDF of the full decision](https://www.gegevensbeschermingsautoriteit.be/publications/beslissing-ten-gronde-nr.-21-2022-english.pdf).

This decision will impact hundreds of companies that use this data, and looks to be another step forward in the battle against the obnoxious plague of cookies and pop-ups.
