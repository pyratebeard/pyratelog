Self-hosting is the idea that instead of using services provided by companies, you own and operate everything yourself.

Reasons to do this can vary from technical curiosity to the desire for data protection.

Some argue in order to be a true self-hoster you should own the physical hardware and run it at home.  Others say that even using cloud providers such as Digital Ocean or Hetzner can be considered self-hosting if you are using a Virtual Private Server (VPS) and no Software as a Service (SaaS).

Understandably some people take the "self" part seriously.  There are others who can't afford the upfront costs or even the running costs.

Do you self-host?  Do you have your own physical servers  serving your sites and data, or do you use a VPS to host; is the latter really self-hosting?

In my opinion, you can refer to running your website or blog on a VPS as self-hosting.  If you have created the VPS, chosen the OS, are the only user with root access, and installed, configured and are managing everything yourself then are you not self-hosting?

Maybe I say that because it is what I do.  I don't host anything public on my own infrastructure.  This blog and my various other online sites are running in the cloud.  I have hosted websites from my home in the past.  Having moved around a lot as part of my previous job it wasn't sustainable so I migrated to running everything public to cloud services.

Some people do run their own servers, and it is something I am interested in doing again now I have settled more.  If you want to own the infrastructure you have to consider not only the purchasing and running costs, but the physical space required.  I met somebody years ago who had a rack in their garage which they were slowly filling with blades.  The hardware geek in me got quite giddy.

Running the few sites I have wouldn't actually have to require much infrastructure.  I could probably get a way with a few Raspberry Pis for the static HTML and maybe a tower server or two for anything requiring more resources.

One reason I like using a cloud provider is the control over the network, specifically the floating IPs you can purchase.  This is not something I could have if I hosted the hardware myself, my current ISP doesn't provide a static IP as an option.  I have had an ISP in the past which would give you a static IP for a price, this was useful for things like my VPN.

If you self-host on your own hardware I would love to hear about what you use, how you find maintaining everything (especially IPs potentially change at random), and even why you decided to go that route.

If you host in the cloud it would be interesting to hear how complex you built the infrastructure, and do you have multiple cloud providers or stick with just one?

You can find ways to contact me on my [homepage](https://pyratebeard.net) if you would like to tell me your self-hosting journey.
