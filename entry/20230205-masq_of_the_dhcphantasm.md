The other day I wanted to test a new network device without connecting it to my home LAN.  I knew the device was preconfigured to use DHCP so I very quickly set up one of my laptops as a DHCP server using [dnsmasq](https://dnsmasq.org/){target="_blank" rel="noreferrer"}.

After making sure the `dnsmasq` package was installed on the laptop I disconnected it from the WiFi and plugged it into a switch.

Configuring a static IP differs from system to system, but in this case I was using `netctl`.  I took a copy of the example configuration file, /etc/netctl/examples/ethernet-static, put it into the /etc/netctl directory, and changed the 'Interface' and 'Address' variables
```
Description='A basic static ethernet connection'
Interface=enp0s25
Connection=ethernet
IP=static
Address=('192.168.1.1/24')
#Routes=('192.168.0.0/24 via 192.168.1.2')
Gateway='192.168.1.1'
DNS=('192.168.1.1')

## For IPv6 autoconfiguration
#IP6=stateless

## For IPv6 static address configuration
#IP6=static
#Address6=('1234:5678:9abc:def::1/64' '1234:3456::123/96')
#Routes6=('abcd::1234')
#Gateway6='1234:0:123::abcd'
```

Bringing the interface up with
```
sudo netctl start ethernet-static
```

The dnsmasq configuration file is usually /etc/dnsmasq.conf, or you can drop your own config files into /etc/dnsmasq.d/ (ensure you uncomment the include line in /etc/dnsmasq.conf).

My setup was very simple
```
# specify ethernet device to operate on
interface=enp0s25

# disable dns
port=0

# configure dhcp range and lease time
dhcp-range=192.168.1.10,192.168.1.12,8h
```

I didn't need to set a netmask as I am putting the DHCP server (my laptop) in the same range.  Before starting `dnsmasq` it is a good idea to validate the configuration
```
dnsmasq --test
```

After starting the `dnsmasq` service I could plug my network device into the router and monitor for any assigned IPs in the file /var/lib/misc/dnsmasq.leases.

There is much more you can do with `dnsmasq` if you want to use it for DHCP properly, but this shows how quick it can be to run it on a LAN or lab environment.
