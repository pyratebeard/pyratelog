Recently I migrated from [gitweb](https://git-scm.com/book/en/v2/Git-on-the-Server-GitWeb) to [stagit](https://codemadness.org/stagit.html) for my [personal git](https://git.pyratebeard.net) server webpage.  I liked gitweb and used it for a number of years, but after seeing [some](https://git.z3bra.org/) [other](https://git.2f30.org/) stagit git sites I preferred the style and decided it was time for a change.

My git server is using `git daemon` to serve the repos.  This is really easy to set up following the [Git on the Server docs](https://git-scm.com/book/en/v2/Git-on-the-Server-Git-Daemon).

Stagit doesn't take much to get working either. As detailed in the [README](https://codemadness.org/git/stagit/file/README.html), to get started clone the stagit repo then use `make` to build and install.  This then gives you the `stagit` command for creating the web content and `stagit-index` for generating the index.html page.

In order to make sure stagit is kept up to date I use a git [post-receive](https://git-scm.com/docs/githooks#post-receive) hook in all my repos which runs every time I push code to the server.  This will keep my git web site up to date automatically.  An example [post-receive script](https://codemadness.org/git/stagit/file/README.html#l97) is shown in the README.

I have been really happy with stagit since moving to it and if you're looking to host your own simple git server I recommend giving stagit a shot.
