## the earth that was
The history of the Internet is a fascinating story.  In the relatively short time it has been around, dating back to the 1960s, it has become one of the greatest technological achievements in human history.

The Internet began decentralised, free from central control and ownership.  Most of the Internet is still decentralised but there is an ongoing fight to stop this from changing.  Governments and companies are all trying to get a handle on the internet and make sure people use it the way _they_ want.  If you rely on one company, what happens when they decide to change their policies, or simply disappear?

What if, instead of uploading all your content to a centralised platform, such as a single social media platform, you could run your own little copy of that social media platform?  It could be made available to everyone, or to only friends and family.  You have more control over it.

So groups of people break away from the centralised social media platform.  Create their own instance to post content amongst their friends, and these little silos are created.

Once you've seen all the cat pictures and memes your friends have, you may want to break out of your instance.  You find another group of friends that had also broken away from the main centralised social media platform, but you don't want to have to create an account on their instance when you already have one on yours.  What if the two instance could be linked so content could be shared without having to join every instance?  This is known as federating, and has been very common in technology.

Take email, for example.  Do you think email would be the same if GMail users could **only** email other GMail users?  Email is based on a protocol which anybody can use.  So GMail can send to Protonmail, because they both use the same protocol.

Another analogy is the telephone network.  Imagine picking up the phone to call a friend only to find out they use a different provider, so you can't talk to them.

Now we have decentralised our social media, people or groups can have more control over their data.  They can also federate with other instances of the same social media platform and build a little web of content.

## no power in the 'verse
Social media using a decentralised federation model already exists.  There are popular alternatives to Twitter ([Mastodon](https://joinmastodon.org){target="_blank" rel="noreferrer"}), Instagram ([Pixelfed](https://pixelfed.org){target="_blank" rel="noreferrer"}), Reddit ([Lemmy](https://join-lemmy.org){target="_blank" rel="noreferrer"}), and more.  These alternatives allow you to join instances or create your own and then federate with other instances.  A number of them also take it one step further and federate across different platforms.  Imagine being able to follow an Instagram account from your Twitter account without having to sign up to Instagram.

One of the most popular protocols currently enabling this federation is [ActivityPub](https://activitypub.rocks/){target="_blank" rel="noreferrer"}.  If you join a Mastodon instance you can follow users from Pixelfed instances, Lemmy instances, [Peertube](https://joinpeertube.org){target="_blank" rel="noreferrer"} instances (a Youtube alternative), as long as the instances are federated.  This has become known as the Fediverse.

Admittedly, joining the Fediverse can seem quite daunting.  I joined Mastodon in 2017.  I did spend some time looking at the [instance list](https://instances.social/list#lang=&allowed=&prohibited=&min-users=&max-users=){target="_blank" rel="noreferrer"} but ultimately I signed up to the most popular instance, [mastodon.social](https://mastodon.social){target="_blank" rel="noreferrer"}.  If you have used Twitter then you should find Mastodon easy to use.  One notable difference is the Local Timeline and the Federated Timeline.  The Local Timeline is every "toot" made from the instance you are logged into.  The Federated Timeline is every "toot" made from any instance that is federated with the instance you're on.

I use hashtags as a way to find content and users to follow. Search results should include all federated instances, giving you access to all the cat pictures and memes in the 'verse.

Apart from Mastodon I have also recently started using Lemmy as a replacement to Reddit.  I have been making a point of catching up on the Fediverse before looking at Twitter or Reddit.

No social media is ideal for all users, but with decentralised instances you can find users with a common interest, or with values that you deem important , or even start your own instance.  And by federating these instances you are not going to be stuck in a silo.

I am by no means an expert on the Fediverse, but if you have any questions feel free to reach out using the contact information on my [home page](https://pyratebeard.net){target="_blank" rel="noreferrer"}.  I would also be happy to hear any suggestions and recommendations of Fediverse accounts to follow.
