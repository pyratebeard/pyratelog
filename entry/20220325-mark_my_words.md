For a long time I was constantly leaving browser tabs open for months at a time.  If I saw an article that I didn't have time to read, or a guide which I wanted to refer back to I would just leave the tab open and either never go back to it or struggle to find the correct tab.

My browser tabs reached over a hundred open tabs for a long time, and I was terrified of it crashing and being unable to restore.

It was time I made use of bookmarks.  My browser of choice is normally [qutebrowser](https://qutebrowser.org){target="_blank" rel="noreferrer"} or Firefox, although I have tried others along the way.  I wanted a bookmark manager that was browser agnostic, and being such an avid user of command line tools I opted for [buku](https://github.com/jarun/buku){target="_blank" rel="noreferrer"}.  `buku` is a great tool and really easy to use.  It has the ability to add tags, custom titles and descriptions, as well as easily export in various formats.

These days I keep a minimum number of tabs open, generally trying to close them all at the end of the day or whenever I log off.  I make heavy use of the tags in `buku`, and one method I have adopted is a "readlist" tag.

Whenever I find an article I want to read but don't have the time to read it immediately I add it to `buku` with the "readlist" tag.  I then run a simple script to routinely export all the URLs tagged with "readlist" and create a simple HTML file, which is then hosted on my webserver.  If I have some time I can pull up this list and pick an article to read.

I would like to figure out a better way to manage my `buku` from another device, like my phone.  I have written a messy syncing script but usually I will `ssh` to my PC and add into `buku`.

Another idea I have been mulling over is being able to search and select bookmarks from my startpage.

Using bookmarks has relieved me of the stress of so many open tabs, while making it much easier to organise and search for links.

Are you a "keep the tab open" type, or do you use a bookmark manager?
