It is common for system administrators to be thought of as strict gatekeepers, not allowing access to anybody.

In my experience nobody bats an eye when you're unable to use the Administrator account on a company laptop, but having to run `sudo` without simply switching to root on a Linux server is absurd.

The hardening and security of Linux servers is very important, yet so many people and companies fail to restrict access correctly.  I have always been an advocate of enforcing the correct use of sudo, specifying commands instead of allowing full root access.  Command exclusion is equally important, and in my opinion often overlooked.  A good sudoers configuration will exclude users from running commands that could potentially allow them to get a root shell, this includes excluding all shells, management tools such as `chmod`, and even commands like `vim` and `tar`, which can easily be used for privilege escalation.

## ed, edd, and edit
Excluding `vim` and `vi` poses an issue when users do need to edit files on the system.  Thankfully the contributors to `sudo` came up with a solution, the `-e` option or `sudoedit`.

The `sudoedit` option allows a user to edit files by making a temporary copy owned by the user.  To give an example, lets say we have a user that requires permission to edit the Apache config file.  They can be granted access in /etc/sudoers, or my preferred method is to put a file into /etc/sudoers.d/.  The line would look like this
```
bob	ALL=(ALL) sudoedit /etc/httpd/conf/httpd.conf
```

The user can then edit the file in one of two ways
```
sudo -e /etc/httpd/conf/httpd.conf
sudoedit /etc/httpd/conf/httpd.conf
```

This creates a copy of the file in /var/tmp, such as `"/var/tmp/httpdXXJkszaG.conf"`, which is owned by the user.  Once the changes have been made the file can be saved and exited as normal.

Wildcards can also be used to specify whole directories or multiple files
```
bob	ALL=(ALL) sudoedit /etc/httpd/conf/httpd.conf, sudoedit /etc/httpd/conf.d/*.conf
```

## (e)u:4(ia)
Recently I had a user who required access to read some of the system log files so they could debug an issue.  As expected I didn't want to grant them sudo permission, overkill for reading some files.  Instead I opted to use the file access control list tool `setfacl`.  To grant a use read permissions to a file incant
```
sudo setfacl -m u:bob:r /var/log/messages
```

Group permissions can also be set with
```
sudo setfacl -m g:bob:r /var/log/messages
```

To view a file's ACLs incant
```
sudo getfacl /var/log/messages
```

The output will look something like this
```
getfacl: Removing leading '/' from absolute path names
# file: var/log/messages
# owner: root
# group: root
user::rw-
user:bob:r--
group::---
mask::r--
other::---k
```

Removing a user's access once they no longer need it is done with the `-x` option
```
setfacl -x u:bob /var/log/messages
```

The use of ACLs means you don't need to modify the file or directories ownership but can still safely give users the permissions they require.

#### rotate me right round
Be careful if setting ACLs on log files as the permissions will be lost when the logs are rotated.  If you require persistence add the `setfacl` line(s) to the relevant logrotate config file.

## go forth and sudo
Using `sudo` should not be shied away from, but it should also not be used in an all or nothing manner.  It may annoy your users but you will rest well at night knowing that you have control and no one can take that from you, something, something, gatekeeper.
