In work we use Ansible for configuration management and various scheduled check (through AWX).  I am a big fan of Ansible and have written plenty of {roles,playbooks} to help manage our Linux infrastructure.

Occasionally though I may need to run a check on a system, or maybe gather some data.  Running a single bash command or a small script can be far quicker than writing an Ansible playbook.  To do this on multiple systems I use to pass commands to ssh in a for loop, but this can become cumbersome.

Recently I was introduced to `drist`.  Like Ansible it is a "tool to configure and synchronize configurations to servers", and uses `ssh` to access the systems.  Unlike Ansible it uses simple shell scripts and `rsync`.

You can download `drist` from bitreich.org
```
git clone git://bitreich.org/drist/
```

The repository comes with plenty of examples to get you started.

If you have a file called "script" in your $PWD you can run it against a single system
```
drist user@hostname
```

Or add a list of hostnames to a file, called "servers" for example, then incant
```
drist servers
```

It is encouraged for you to have ssh keys configured on your servers, otherwise you will be prompted to enter your password multiple times for each system.

An example script could be as simple as gathering some basic information
```
hostname
uname -r
cat /proc/loadavg
who
```

We can then incant `drist <hostname>`
```
Running on blacksun
Executing file "script":
blacksun
4.9.0-8-amd64
0.00 0.00 0.00 1/92 21836
pyratebeard pts/0        Sep 23 10:06 (mosh [12511])
```

You can also copy up files and then use the script to carry out any actions required.  For example, lets say we want to set a new MOTD on our servers.  Create a directory called "files" and then under that directory add a new MOTD file
```
.
├── files/
│   └── motd
└── script
```

Our script now looks like this
```
sudo mv ./motd /etc/motd
sudo chown root:root /etc/motd
sudo sed -i 's/^PrintMotd\ no/PrintMotd\ yes/' /etc/ssh/sshd_config
sudo systemctl reload sshd
```

We move the MOTD file to /etc and change ownership to root.  Make sure we have PrintMotd enabled in sshd_config and reload the service to pick up any changes.

These are some very basic examples but hopefully you can see how quick and easy it is to use, especially for small tasks were Ansible might be considered overkill.
