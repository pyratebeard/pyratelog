As a lot of people are doing during the COVID-19 pandemic I am working from home a lot more.  After the first week I decided my desk needed brightening up a bit.  I picked up some cheap comic books from my LCS and set about covering my desk.

![before](/img/20200323-comic_desk-01-before.jpg#fitwidth)
_obligatory before photo_

My desk is a worktop/legs combo from Ikea so the legs screw off really easily. I cut out pages and panels from the comics, I tried not to weep while I destroyed them...

![before](/img/20200323-comic_desk-02-begin.jpg#fitwidth)

I spent a while laying the pages out to make sure the desk was covered and I had a good mix of styles.  Once they were glued down I brushed on 5 coats of a clear sealer, leaving 20 minutes inbetween coats.

![before](/img/20200323-comic_desk-03-ready_to_seal.jpg#fitwidth)

The completed desk, dry and back in place.

![before](/img/20200323-comic_desk-04-sealed_complete.jpg#fitwidth)

And the after photo.  I'm really happy with how it turned out, and should make working from home a bit more interesting.

![before](/img/20200323-comic_desk-05-after.jpg#fitwidth)

