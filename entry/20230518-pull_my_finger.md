The other day I read a blog post by drkhsh about [using fingerd](https://drkhsh.at/2023-05-15_finger.html){target="_blank" rel="noreferrer"} to display contact details in the terminal.

This looked pretty cool, and is geeky in a retro Unix way, so of course I had to do it as well.

You can now finger me remotely!  To do so incant
```
finger pyratebeard@pyratebeard.net
```

If you don't have `finger` installed you can also use `nc` (netcat)
```
echo pyratebeard | nc pyratebeard.net 79
```

While drkhsh's post shows the setup on OpenBSD I am using Devuan so it was a little different.  These steps should also work on Debian or any other deb package system.

I needed to install the `fingerd` daemon, the `finger` command, and `xinetd`, which will run the daemon
```
sudo apt -y install fingerd finger xinetd
```

Then created an `xinetd` service config file under /etc/xinetd.d/
```
service finger
{
	socket_type	= stream
	protocol	= tcp
	wait		= no
	user		= nobody
	server		= /usr/sbin/in.fingerd
	server_args	= -u -p /home/pyratebeard/finger
	disable		= no
}
```

The `server_args` option tells `fingerd` to require a username to finger and use an alternative script, in my case /home/pyratebeard/finger.

The script I point to is what outputs when you `finger` me
```
#!/bin/sh
cat <<EOF
                          __         __                        __
  .-----.--.--.----.---.-|  |_.-----|  |--.-----.---.-.----.--|  |
  |  _  |  |  |   _|  _  |   _|  -__|  _  |  -__|  _  |   _|  _  |
  |   __|___  |__| |___._|____|_____|_____|_____|___._|__| |_____|
  |__|  |_____|

  ----------------------------------------------------------------
            |
        web | https://pyratebeard.net
            |
    address | root@pyratebeard.net
        irc | pyrate > unix.chat | pyratebeard > {darkscience,libera,oftc}
            |
     pgp id | 0x5113a16d
     pgp fp | 7a8e 129d be67 9dab aeb3 37c5 c787 7c71 5113 a16d
     pgp dl | https://pyratebeard.net/pgp.pub
            |
    ssh key | ssh-ed25519 aaaac3nzac1lzdi1nte5aaaaicsluiy54h5flgxnnxqifwpnfvknih1/f0xf0ycthdqv
            |
   id proof | https://keyoxide.org/7a8e129dbe679dabaeb337c5c7877c715113a16d
            |
  ----------------------------------------------------------------

  finger ring:

      * drkhsh@drkhsh.at

EOF
```

The script has to be executable, then I restarted the `xinetd` service.

If you try this also make sure port 79 is allowed through your firewall, this is the default `fingerd` port.

A few of us on IRC have now started a "finger ring", so if you think this is fun and want to join in let me know using the contact details on my [homepage](https://pyratebeard.net){target="_blank" rel="noreferrer"}, or `finger` me, or jump into the #unix channel on [unix.chat](https://unix.chat){target="_blank" rel="noreferrer"} and post your finger link.
