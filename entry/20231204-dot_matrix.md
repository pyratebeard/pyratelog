A user's dotfiles are sacred.  I, like many others, try to maintain all of my custom configurations and scripts in a [git repo](https://gitlab.com/pyratebeard/dotfiles){target="_blank" rel="noreferrer"}.

This is a great when you need to build a new system, just clone and go.

I have a number of different systems that I clone my dotfiles onto - PC, laptop, [phone](https://log.pyratebeard.net/entry/20170614-termux_on_android.html){target="_blank" rel="noreferrer"}, and my servers.

All of these systems can be slightly different.  Maybe I'm running a different OS, like OpenBSD, so commands or filepaths are dissimilar.  To deal with this I would create a new branch as soon as I clone my repo specifically for that device and make any slight adjustments.

Over time I would modify or improve my dotfiles, but I wouldn't always push the changes straight away.  I let things build up until I finally sit down, switch back to my main branch, and pick through the changes to push without including any device specific adjustments.

This is a pain.  So I have decided to spend some time improving things meaning I can stick to my main branch on all devices.

#### off the chain
[b78637c91813483f9a9b80899c4f5d9ede5c14ed](https://git.pyratebeard.net/dotfiles/commit/b78637c91813483f9a9b80899c4f5d9ede5c14ed.html){target="_blank" rel="noreferrer"}
```
diff --git a/zsh/.zsh/keychain.zsh b/zsh/.zsh/keychain.zsh
@@ -1,13 +1,16 @@
 # KEYCHAIN
 
-# ssh key dir
-SSH_KEY_DIR="$HOME/.ssh"
+## don't run if keychain not installed (i.e a server)
+if command -v keychain >/dev/null ; then
+	# ssh key dir
+	SSH_KEY_DIR="$HOME/.ssh"
 
-GPG_TTY=$(tty)
-export GPG_TTY
+	GPG_TTY=$(tty)
+	export GPG_TTY
 
-# funtoo keychain
-eval $(keychain -q --agents gpg --nogui --eval 0xC7877C715113A16D)
-if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ] ; then
-	export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
+	# funtoo keychain
+	eval $(keychain -q --agents gpg --nogui --eval 0xC7877C715113A16D)
+	if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ] ; then
+		export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
+	fi
 fi
```

On my servers I don't tend to need GPG but my zsh config includes running `keychain` to set up gpg agent when a terminal is started.  I modified my _keychain.zsh_ file to only run if `keychain` is installed.

#### tmux, ahoy!
[8af62ecd9c7e44a77a11541b558fe1ca069a6e14](https://git.pyratebeard.net/dotfiles/commit/8af62ecd9c7e44a77a11541b558fe1ca069a6e14.html){target="_blank" rel="noreferrer"}
```
diff --git a/zsh/.zsh/welcome.zsh b/zsh/.zsh/welcome.zsh
@@ -1,4 +1,6 @@
 # WELCOME
+## don't run this if over ssh (i.e. a server)
+[[ -v SSH_TTY ]] && exit 0
 
 PTS=$(ps -U $USER | awk '{ print $2 }' | grep "pts/" | uniq | wc -l)
 
@@ -24,4 +26,4 @@ tmux list-sessions >/dev/null 2>&1 || script
 if [[ $TMUX_PANE == "%0" ]] && [[ ! -v VIMRUNTIME ]]; then
	$HOME/bin/ahoy
 fi
-cat ~/tmp/logo5
+#cat ~/tmp/logo5
```

My zsh setup also includes a script which starts `tmux` on the first terminal opened and then runs my custom `ahoy` script.  Again, I don't want or need this on my servers so I modified it not to run over ssh.

My `ahoy` script works well on my main system but needs work to run without errors on my laptop.  Mainly ignoring backups and mail local to my PC.

#### don't prompt me
[056f2d6ddfdab3b9345bd8dd7b65430defa8f0e2](https://git.pyratebeard.net/dotfiles/commit/056f2d6ddfdab3b9345bd8dd7b65430defa8f0e2.html){target="_blank" rel="noreferrer"}
```
diff --git a/zsh/.zsh/prompt.zsh b/zsh/.zsh/prompt.zsh
@@ -99,8 +99,11 @@ PROMPT='${USER_LEVEL}[${COLOR_NORMAL}%~${USER_LEVEL}]$(GIT_PROMPT)── - %f'
 tiny)
 #PROMPT='%F{3} %%${COLOR_NORMAL} '
 # change prompt colour if started from vim
-if [[ -v VIMRUNTIME || -v SSH_TTY ]] ; then
-	PROMPT='%F{9} ──── ─${COLOR_NORMAL} '
+if [[ -v VIMRUNTIME ]] ; then
+	PROMPT='%F{13} ──── ─${COLOR_NORMAL} '
+# change prompt to show hostname if over ssh
+elif [[ -v SSH_TTY ]] ; then
+	PROMPT='%F{13} ${HOSTNAME}%F{3}_ ${COLOR_NORMAL}'
 else
	PROMPT='%F{11} ──── ─${COLOR_NORMAL} '
 fi
```

My (current) zsh prompt is pretty minimal, showing `──── ─` on the left then the filepath and git branch details on the right.  For my servers I changed the prompt to show the hostname.  Now I have configured the prompt to automatically change to the hostname if connecting over `ssh`.  This is actually much nicer as my prompt was the same on my laptop and phone and sometimes I forgot if I was still in an `ssh` session.

For [a while](20221020-shell_shocked.html){target="_blank" rel="noreferrer"} now I have been making use of the terminal buffer in `vim` so set a different colour prompt for that.


#### hulk bash!
[9aa4aee5b7a121c0af42de4bf730e8f23e8166f8](https://git.pyratebeard.net/dotfiles/commit/9aa4aee5b7a121c0af42de4bf730e8f23e8166f8.html){target="_blank" rel="noreferrer"}
```
15 files changed, 69 insertions(+), 13 deletions(-)
```

This one is probably good practice anyway.  Switching all my `#!/bin/bash` crashbangs to `#!/usr/bin/env bash` because some systems, like the BSDs, use a different filepath for `bash`. (69 insertions, nice! *snigger*)

#### moniker
[58fa110b56b090fbb378a16ad8df5a812588a20b](https://git.pyratebeard.net/dotfiles/commit/58fa110b56b090fbb378a16ad8df5a812588a20b.html){target="_blank" rel="noreferrer"}
```
diff --git a/zsh/.zsh/aliases.zsh b/zsh/.zsh/aliases.zsh
@@ -11,13 +11,19 @@
 #  author ▓▒ pyratebeard
 #    code ▓▒ https://git.pyratebeard.net/dotfiles/file/zsh/.zsh/aliases.zsh.html
 
-    alias sudo='sudo ' # hack to allow aliases with sudo
+# ▓▓▒░ root
+# check for doas so aliases can be used on different systems
+# add whitespace for hack to make aliases woth with {sudo,doas}
+command -v doas >/dev/null && \
+    alias sudo='doas ' || \
+    alias sudo='sudo '
 
 # ▓▓▒░ sys

[output ommitted]

 # ▓▓▒░ fun(ctions)
@@ -219,6 +225,6 @@
         pandoc -s -t man "$*" | man -l -
     }
 
-    :q() {
-        [[ -v SSH_TTY ]] && echo dumpshock || sudo systemctl poweroff
+    :q!() {
+        [[ -v SSH_TTY ]] && echo dumpshock || sudo halt -p
     }
```

Aliases are important, there were a couple of changes required here.  First some systems use `doas` instead of `sudo` so I put in a check for `doas` and if it exists I alias it to `sudo`.  That way all my aliases that are configured to use `sudo` don't break.  My shutdown function also had to be modified to work on any platform, so I switched to directly calling `halt`.

#### epithet
[255622b7c1b8e68529be337126bfdbd9575c609e](https://git.pyratebeard.net/dotfiles/commit/255622b7c1b8e68529be337126bfdbd9575c609e.html){target="_blank" rel="noreferrer"}
```
diff --git a/zsh/.zsh/aliases.zsh b/zsh/.zsh/aliases.zsh
@@ -18,10 +18,26 @@ command -v doas >/dev/null && \
     alias sudo='doas ' || \
     alias sudo='sudo '
 
+# ▓▓▒░ unix
+# openbsd's ls(1) doesn't provide the `--color` option.
+# i have grown to like this and spent a long time
+# trying to find a work around, but none exists tiko.
+# so i succumbed to installing coreutils and doing this
+command -v gls >/dev/null && \
+    alias ls="gls -hF --color=auto" || \
+    alias ls="ls -hF --color=auto"
+
+# i also need dircolors(1) from coreutils for zsh autocompletion
+command -v gdircolors >/dev/null && alias dircolors="gdircolors"
+
+# i make use of some funky shit in my log makefiles
+# which isn't possible using the openbsd make(1)
+command -v gmake >/dev/null && alias make='gmake'
+
+
 # ▓▓▒░ sys
```

More alias issues, this time with `ls`.  I have grown to like colour output with `ls` but the OpenBSD package doesn't provide the `--color` option.  I attempted to find a workaround but instead succumbed to installing the coreutils package which includes `gls`, and thus providing `--color`.  I then check which package is installed to set my alias.

The same check is configured in my `chpwd_auto_cd` function as it didn't seem to pick up the alias.  If you're interested, [this function](https://git.pyratebeard.net/dotfiles/file/zsh/.zsh/functions/chpwd_auto_cd.html){target="_blank" rel="noreferrer"} auto runs `ls` when I `cd` into a directory.

The coreutils package also gives me `gdircolors`, a substitute for `dircolors`, which is used in my zsh autocompletion settings.

#### mainland
These small changes mean I don't have to make device specific modifications to my dots, which in turn will (hopefully) help me to keep my repo more regularly updated.

There are still a few updates that required to make it close to perfect.  As mentioned, my `ahoy` script doesn't run properly on any device other that my PC.

I also use a script for `tmux` on my laptop which displays battery information.  I want to work on this and make it cross-device compatible.

How do you manage your dots across devices?
