For those that follow me on Mastodon or Twitter you may have seen a #weeklymusictoot being posted on my feed over the last few weeks.

This is an automated post which lists some of the music I listened to that week.

My music player of choice is [cmus](https://cmus.github.io/){target="_blank" rel="noreferrer"} and one of the cool things it can do is run scripts on a song change or when the status changes (pause, play, stop, etc.).

I have seen people use this to display album artwork or send a desktop notification each time a song changes.  There are plenty of examples on the cmus [wiki](https://github.com/cmus/cmus/wiki/status-display-programs){target="_blank" rel="noreferrer"}.  In the cmus repository there is a script called `cmus-status-display` which outputs the currently playing song to a text file.  I made some modifications to the script to display the output in my preferred way, or to display the name of the playlist the song is in if applicable.

```
#!/bin/sh

output()
{
	# write status to ~/cmus-status.txt (not very useful though)
	echo "$*" >> ~/.cmus/wmt.txt 2>&1
}

while test $# -ge 2
do
	eval _$1='$2'
	shift
	shift
done

if test -n "$_file" && test -n "$_title" && [[ "$_status" == "playing" ]] ; then
	if [[ $(cmus-remote -Q | grep "set play_library" | awk '{print $NF}') == "false" ]] ; then
		_underscore=$(echo $_title | sed 's/\ /_/g')
		_playlist=$(grep -i -e "$_title\|$_underscore" ~/.cmus/playlists/* | awk -F: '{print $1}' | awk -F/ '{print $NF}' | head -n1)
		test -n "$_playlist" && output "custom $_playlist playlist"
	elif [[ $(cmus-remote -Q | grep "set play_library" | awk '{print $NF}') == "true" ]] ; then
		output "$_album by $_artist"
	fi
elif test -n "$_url" && [[ "$_status" == "playing" ]] ; then
	output "$_url"
fi
```

Then I created a script which is run each Friday via cron to take the contents of the music list and send out a toot on mastodon.

```
#!/bin/sh
#                              ██
#                             ░██
#   ███     ██  ██████████   ██████
#  ░░██  █ ░██ ░░██░░██░░██ ░░░██░
#   ░██ ███░██  ░██ ░██ ░██   ░██
#   ░████░████  ░██ ░██ ░██   ░██
#   ███░ ░░░██  ███ ░██ ░██   ░░██
#  ░░░    ░░░  ░░░  ░░  ░░     ░░
# w e e k l y   m u s i c   t o o t

# output of cmus-status-display script
music_list="$HOME/.cmus/wmt.txt"

# toot command path
toot="$HOME/src/warez/toot/bin/toot"

# sort list by number of plays
weekmusic=$(sort $music_list | uniq -c | sort -nr | \
			awk '{$1=""; print $0}' | \
			tr '[:upper:]' '[:lower:]' | \
			sed 's/^/\*\ /')

# generate toot and count characters
char=$(cat <<wmt | wc -m
(automated) #weeklymusictoot

this week i listened to:

$weekmusic
wmt
)

# if characters is more than 500 the toot will fail
# we delete the last entry in the list until
# there are less than 500 chars
until [ ${char} -lt 500 ] ; do
weekmusic=$(echo "$weekmusic" | sed '$d')
cat <<wmt
$weekmusic
wmt
char=$(cat <<wmt | wc -m
(automated) #weeklymusictoot

this week i listened to:

$weekmusic
wmt
)
sleep 1
done

# generate toot then delete list for next week
# if it fails page me
cat <<wmt | $toot post && rm -f ${music_list} || curl -d "wmt failed to send" https://pager.pyratebeard.net/wmt
(automated) #weeklymusictoot

this week i have been listening to:

$weekmusic
wmt
```

The second week this ran I found that the toot failed as the character count was greater than 500.  I amended the script to sort the list by most entries, i.e. most played album, then count the characters and remove the least played album until the toot is viable.

My Mastodon and Twitter accounts are linked using [Mastodon Twitter Crossposter](https://crossposter.masto.donte.com.br/){target="_blank" rel="noreferrer"}, so soon after my automatic toot goes out it is tweeted as well.

There is no real reason for this except being a bit bored for an hour so writing a tooting script.  But maybe somebody will discover some good music from it ¯\_(ツ)_/¯ .
