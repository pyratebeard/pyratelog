We are almost at the end of 2021, and as we all know it has been a crazy couple of years.

I am not usually one for new year resolutions but yet again I feel like I should put more effort into this blog.  This time I have a plan...

Recently I came across the [100DaysToOffload](https://100daystooffload.com) challenge and while my time is even more precious since becoming a parent, I would like to give this a shot.  A post roughly every 3 days seems like a lot, so I probably won't be doing long how-to guides, although I will try to put some in.  I am also going to venture outside the realm of "tech" and try to include other passions of mine.  This will help keep the posts coming... probably.

If anybody has any suggestions for posts please let me know, you can find contact information on my [homepage](https://pyratebeard.net).

So here's to 2022 and 100 ramblings from yours truly.
