After getting the hang of [buffers](https://log.pyratebeard.net/entry/20230412-buffer_me_up.html) in [Vim](https://www.vim.org/) I looked at [registers](https://www.tutorialspoint.com/vim/vim_registers.htm).

As with other text editors it is possible to cut (yank) and paste content.  When something is yanked in Vim it is entered into a register.  There are different registers available for use including the letters `a-z` and numbers `0-9`.  If a register is not specified then Vim will use the unnamed register, `""`.

If you have used Vim already then you have probably made use of the unnamed register any time you yanked and pasted anything.  A habit I started to develop was making use of the other registers available to me.

To yank to one of the other registers specify it with double quotes then the register before yanking.  For example, to yank a word to register 5 highlight the word then incant `"5y`.  To paste from a register other than the unnamed is the same except using `p` instead of `y`, for example `"5p`.

Deleting (`d`) also invokes yank, so keep that in mind if your unnamed register doesn't contain what you're expecting.

To view the registers incant
```
:registers
```

As with buffers I use a leader keymap in my ~/.vimrc
```
nnoremap <leader>r :registers<CR>
```

Now I can view my registers quickly with `\r`.

This has only been a brief overview of registers.  I will touch on them a bit more in the third post of this series when I discuss macros.

To learn more about registers incant
```
:help registers
```
