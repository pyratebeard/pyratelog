Recently I decided to try and reduce the amount of "doom scrolling" I was doing on my phone.

While my desktop PC is still my main computing device it is inevitable that the computer I carry around in my pocket will get a high degree of use.  The problem being that "use" was not particularly productive a lot of the time.

Like most phone users it is all about apps.  The main culprits in my case were [Boost](https://boostforreddit.com/){target="_blank" rel="noreferrer"} for Reddit, [Twidere](https://twidere.com/){target="_blank" rel="noreferrer"} for Twitter, and [Fedilab](https://fedilab.app/){target="_blank" rel="noreferrer"} for Mastodon.

My Twitter usage had dropped dramatically since I started to focus more on Mastodon and the Fediverse.  Getting rid of Twidere became even easier with [Mastodon Twitter Crossposter](https://crossposter.masto.donte.com.br/){target="_blank" rel="noreferrer"}, which tweets my toots for me.

Reddit had been my main weakness.  My Reddit usage is mostly lurking in my favourite subs and checking the popular posts.  The time I spent scrolling down and down kept increasing, I felt like there would be a good meme or post just a bit further down so I kept going.

My most browsed subs were already in my RSS feed (more on this later) so I decided to remove Boost and rely on the website if I needed to spend time on Reddit.  On my desktop this isn't too bad (long live old.reddit.com!).  On mobile the old view is unusable so the new style it is (although I wish they would remove the "This page looks better in the app" banner).

Within days I found my time on Reddit dropped to practically nothing except for what I have in my RSS feed, which I only check on my desktop.

Fedilab is staying for now, I find I am less likely to doom scroll on Mastodon.  I check my timeline while only occasionally looking at the federated timeline.  This may change as I spend more time on it, for now though I am happy with the amount of usage.

## really simple syndication
RSS is great.  In years gone the RSS icon seemed to be everywhere, these days it doesn't appear to be as popular.  Thankfully it is still around.

About two years ago I discovered that you can add ".rss" to any subreddit URL to get an RSS feed.  For example, https://reddit.com/r/unixart.rss.

The same is true for Mastodon profiles, get my toots in your feed with https://harbour.cafe/@pyratebeard.rss.

Twitter is not as RSS friendly.  As far as I know there is no Twitter native way to get feeds from profiles.  Instead I use nitter.net and add "/rss" to the URL, for example https://nitter.net/pyratebeard/rss.

This has been part of my RSS setup for a long time, and it is enough for me to keep updated without falling into the endless darkness of the scroll.

By removing these social apps from my phone and using RSS or the websites I have found myself looking at my phone less and less.  When I do spend time on my phone it is for something more productive, like writing this blog post.  I am sure there is some irony there.
