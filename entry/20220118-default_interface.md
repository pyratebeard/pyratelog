#### TL;DR
Alias to only show the default interface details
```
alias iip="ip a s $(ip r | grep default | grep -oP '(?<=dev )[^ ]*')"
```

---

I work on a lot of systems these days that run tools like docker or kubernetes, and with them a lot of virtual interfaces exist.  The number of interfaces can be as 10, it can be 200.  Whatever the number, when you want to quickly check the IP address of the system the output of `ip a` can be too much.

If you know the default device name you can incant
```
ip a show <device>
ip a s eth0
```

It use to be that the first ethernet interface was `eth0`, but this isn't always the case these days.  As a Linux sysadmin I mostly deal with Red Hat Enterprise Linux (RHEL) or CentOS systems.  The network interface naming convention was changed in RHEL 7 to use "[consistent network device naming](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/ch-consistent_network_device_naming)".  The system now uses the bus-ID to create _predictable device names_.  The default device name now changes across systems.  Or maybe you don't want to scroll back through the output of 200 interfaces to find your IP.  Or maybe you use bonds on some systems but not others.

Whatever the reason, it is too much effort.  We're sysadmins, our time is precious remember, and that is why I set this alias on my systems
```
alias iip="ip a s $(ip r | grep default | grep -oP '(?<=dev )[^ ]*')
```

Now when I want the default interface details I can run `iip` and be spared all the gumph.

If you know of a sleeker way of doing this, or want to recommend any other good aliases let me know.
