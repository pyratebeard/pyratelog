You have an ssh session on a remote system and your network drops.  Your terminal is now stuck.  We've all been there.

Instead of killing the terminal you can use an ssh escape sequence to terminate the session.  The sequence we want is `~.` (tilde and period).

First key Return, then key the sequence `~.`.  Your terminal is free!

This isn't the only ssh escape sequence.  To see others open an ssh session to a remote system and key `~?`, you will be presented with this message
```
Supported escape sequences:
 ~.   - terminate connection (and any multiplexed sessions)
 ~B   - send a BREAK to the remote system
 ~C   - open a command line
 ~R   - request rekey
 ~V/v - decrease/increase verbosity (LogLevel)
 ~^Z  - suspend ssh
 ~#   - list forwarded connections
 ~&   - background ssh (when waiting for connections to terminate)
 ~?   - this message
 ~~   - send the escape character by typing it twice
(Note that escapes are only recognized immediately after newline.)
```

On occasion I use `~.` when I experience network drop out.  I have used `~^Z` before to suspend my ssh session, useful when you want to leave jobs running and don't have screen or tmux available (see my post "[this ssux](20220331-this_ssux.html) on how I use tmux with ssh).

The other escape sequences I have not used, but it is good to know they are there.
