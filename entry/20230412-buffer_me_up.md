This is one of a few entries I am working on about everyone's favourite text editor, [Vim](https://www.vim.org/){target="_blank" rel="noreferrer"}.  There are plenty of Vim guides and tutorials on the internet already, from first steps to hardcore power user tips.  My entries are going to be somewhat of a middle ground.

I have been using Vim as my main (read "_only_") text editor for many years.  Over this time I have continuously learnt new ways of working and how to use Vim's functions properly.

In the past couple of years I have been working on enhancing my Vim workflow, the first step was to master [buffers](https://vim.fandom.com/wiki/Buffers){target="_blank" rel="noreferrer"}.

When I switched to Vim from [Sublime](https://www.sublimetext.com/){target="_blank" rel="noreferrer"} I had trouble getting over the use of tabs for open files.  Sublime, like a lot of GUI text editors, would open files in tabs at the top of the window.  Vim does have tabs but they are not generally used in the same way.

Opening a file in Vim creates a buffer, by default filling the window.  If another file is opened the new buffer will fill the window, replacing the original buffer.  To view buffers incant
```
:buffers
```

or
```
:ls
```

This will list all open buffers displaying them each with a unique number, one or more indicators, the file in the buffer, and the line the cursor is on
```
  1  a   "entry/vim_-_buffer_me_up.md"  line 9
  5 #h   "pyratelog.sh"                 line 1
```

In this example there are two buffers open, numbered 1 and 5.  The `a` indicator on buffer 1 means it is the active buffer, i.e. what is currently loaded in the window.  The `#` and `h` indicators on buffer 5 mean it is the alternate buffer (`#`) and it is hidden (`h`), i.e. it is loaded but not visible.

The alternate buffer indicator is normally the previously active buffer.  Switching to this buffer is done with `CTRL-^` (that's `CTRL`, `SHIFT`, `6`, although most terminals will do the same with `CTRL`, `6`).  Meaning if two files are being worked on quickly toggle between them using `CTRL-^`.

The unique number of a buffer doesn't change once it is open.  If the number of a buffer is known switch to it using `<number> CTRL-^`, or incant
```
:buffer <number>
```

or
```
:b<number>
```

#### quick tip
In my ~/.vimrc I set the following keymap
```
nnoremap <leader>b :ls<CR>:b
```

With this I type `\b` (backslash is the default 'leader' key) to view my buffers, the prompt will wait for me to type a number and hit enter.  It has made managing a large number of buffers quite easy.  Vim also has a built in auto-complete.  By entering `:b <TAB>` Vim will cycle through the buffers, or start typing part of the filename or filepath and it will auto-complete.

Find out more about buffers using Vim's help pages
```
:help buffers
:help :buffers
```

## split the difference
Vim has the ability to split the window in order to show multiple buffers.  With a buffer already open incant
```
:split <filename>
```

This will split the window horizontally loading the new buffer so both files can be viewed at once.  It is also easy to split a loaded buffer with
```
:sb<number>
```

To vertical split the window incant
```
:vert split <filename>
:vsplit <filename>
```

To vertical split a loaded buffer incant
```
:vert sb<number>
```

Navigating between splits can be done with `CTRL-W` commands, e.g. `CTRL-W h`, `CTRL-W j`, `CTRL-W k`, `CTRL-W l` are left, down, up, right, respectively.

## tabulous
As mentioned previously Vim does have [tabs](https://vim.fandom.com/wiki/Quick_tips_for_using_tab_pages){target="_blank" rel="noreferrer"} but they aren't designed to be used like other text editors.  If there are a few buffers open in splits, maybe opening another file for a quick change, such as a 'scratchpad', could mess up the layout.  Opening a tab will allow another file to be edited without disturbing the split layout.

To open a new file in a tab incant
```
:tabe <filename>
```

Similarly to open a buffer in a tab incant
```
:tab sb<number>
```

Navigating tabs can be done in a number of ways, just like navigating buffers.  To get started, in normal mode `gt` and `gT` will switch to the next and previous tab respectively.

There are other uses for tabs of course, although since learning to use buffers correctly I haven't missed them.

To find out more about tabs incant
```
:help tabs
```

This has only been a brief overview of buffers, splits, and tabs.  For those that found this interesting take a look at the Vim help pages, or online resources such as [Vim Tips Wiki](https://vim.fandom.com/wiki/Vim_Tips_Wiki){target="_blank" rel="noreferrer"}, and embrace Vim as it was designed.
