The other day I thought of a scenario that has kept my mind occupied ever since.

My phone had frozen causing me to reboot.  This doesn't happen very often, it has been a long time since my phone had been off.  On reboot I had to unlock the phone, the password I remembered.  Then once the system had started I had to unlock the SIM card.  I typed in the four digit passcode but it was incorrect.  For codes like this I generally rely on muscle memory, this time my mind was blank.

I went over to my PC and checked my password manager.  Thankfully the code was there and I unlocked the SIM.  Normally when I am away from my PC I can access it remotely using a VPN and `ssh` on my phone (with [termux](https://termux.com/){target="_blank" rel="noreferrer"}).  My password manager of choice is [pass](https://www.passwordstore.org/){target="_blank" rel="noreferrer"} and is stored as a git repository on one of my remote servers.  If necessary I can clone the repository to my phone, my laptop, or even locally on the server.

This is where the problem scenario started to form in my mind.  To access my VPN I need a certificate stored on my phone.  These certificates are generated on the VPN system on a per device basis.

If my phone was off I wouldn't be able to access my home network.  Some of my remote systems have firewall rules to only allow ssh traffic from my home network.  For the couple that aren't I use ssh keys to authenticate, which you've guessed it are on my phone or PC.  I wouldn't even be able to access the firewall settings as I would need a one-time passcode (OTP), which is provided by an app on my phone.

Let us say I am travelling, my phone dies or reboots and I have forgotten the passcode.  I need to access one of my servers or my password manager.  How would I do it?

For the password manager I could use a tool such as Bitwarden and be able to access it via the website using just a passphrase.  This is clearly an obvious and probably quite sound solution.  That little selfhosting devil on my shoulder isn't a fan though.

I could always allow password authentication in my ssh config on my servers, but this isn't as secure as using keys.  I thought about storing an ssh key in Bitwarden so I could download and use it from an unknown system.  Maybe have a few keys and make them one-time use by deleting the entry in ~/.ssh/authorized_keys after each use.

So you are stranded outside your network and systems with no certificates or keys, how would you solve this problem?  Do you already have a solution for your own setup?  Should I reduce security to allow for emergency access?  I am open to any ideas or suggestions, contact details can be found on my [homepage](https://pyratebeard.net){target="_blank" rel="noreferrer"}.
