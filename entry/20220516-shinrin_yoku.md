Yesterday I took part in the Japanese practice of Shinrin Yoku, or "forest bathing".

It is a way of reconnecting with nature by taking the time to experience nature with all of your senses.

The two hour session was run by a friend and was a relaxed, enjoyable way to switch off for a while.

After a short introduction at the edge of a wooded area, we were invited to pick a piece of nature that wasn't still living i.e. no ripping leaves of branches.  Luckily at this time of year there was plenty of foliage on the ground.  With the leaf, twig, pine cone, or whatever we picked, we concentrated on the item with any worries or stresses we had and then placed it down so we could continue our journey, leaving our stresses behind.

Throughout the session we were invited to take part in sensory activities, called "invitations" as they are not compulsory.  These invitations included looking at the shapes and colours around us, listening to the wind in the trees and the birdsong, smelling the air or the leaves of the trees, and so on.  Even taste, albeit with a piece of chocolate.

Parts of the session focused on meditation, controlling our breathing and clearing our minds.  At times we were also invited to discuss or describe what we felt or heard, again this was not compulsory.

It was nice to get away from my tech for a while, I don't get to go hiking or camping much these days so taking the time to immerse myself in nature was satisfying.

You can read more about this practice on the [Nádúr](https://nadurforesttherapy.com/what-is-forest-bathing/){target="_blank" rel="noreferrer"} website.
