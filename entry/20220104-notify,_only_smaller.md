[ntfy](https://ntfy.sh) is an opensource publish-subscribe (pub-sub) notification service.

Publishing a message can be as simple as using HTTP PUT/POST with `curl`.
```
curl -d "Schway, backups successful" ntfy.sh/pyratebackups
```

Publishing can be done in your choice of language (python, php, go, etc.) or via the [Android app](https://ntfy.sh/docs/subscribe/phone/).  Take a look at the [docs](https://ntfy.sh/docs/publish/) for more examples.

Messages can handle titles, priorities, and tags (including emojis).
```
curl -H "Title: pyratebackups" \
     -H "Priority: high" \
     -H "Tags: warning,rotating_light,skull" \
     -d "Frak, backups shit the bed" ntfy.sh/pyratebackups
```

Topics (e.g. "pyratebackups") can be anything you want, but there is no authentication so anybody can subscribe.

Subscribing to topics can be done through the ntfy webUI, the Android app, or using the [API](https://ntfy.sh/docs/subscribe/api/) in your own app.

ntfy can also be [self-hosted](https://ntfy.sh/docs/install/).  Packages are available for Debian/Ubuntu, Fedora/CentOS/RHEL, Arch, as well as a binary and a docker image.

I usually set up an email notification in my scripts, but have been trying `ntfy` out for a few weeks now.  Adding a `curl` command to my scripts then subscribing via the Android app was really simple to do, and now I get instant notifications whole also reducing the noise in my mailbox.

## see also
There are some great notification tools out there, a few to check out are:

* [gotify](https://gotify.net/)
* [apprise](https://pypi.org/project/apprise/)
* [pingme](https://pingme.lmno.pk/#/)

I like the look of apprise so may trial that at some point in the coming months.
