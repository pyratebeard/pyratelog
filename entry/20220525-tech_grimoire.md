### grimoire
(ɡriːmˈwɑː) _noun_

> a textbook of sorcery and magic

---

Keeping notes has been really important since I started using Linux.  At first it was a way to help me remember the simplest things, how to install my distro, setting up my wifi connection, etc.

Over time my notes became a useful reference for commands or processes that I did not use very often, especially when I began my career as a Linux sysadmin.  Years ago I stopped using a paper notebook and started keeping digital notes.  As I was spending so much time in vim I started using [vimwiki](https://vimwiki.github.io/){target="_blank" rel="noreferrer"} for making Markdown notes.

To get started using vimwiki add the plugin to your .vimrc, see [Installation](https://vimwiki.github.io/#installation){target="_blank" rel="noreferrer"} for more details.  You can then start vimwiki using `<Leader>ww` (default Leader is '\\').

One of the big draws of vimwiki is the ability to link pages.  It enabled me to organise my notes in a logical manner and easily navigate through pages.

To make my notes more accessible to me when I am not at my PC I use [Gollum](https://github.com/gollum/gollum){target="_blank" rel="noreferrer"}, which can display Markdown files in a web browser using the built in web interface.  Updates to the files are tracked using Git, when the repo is updated Gollum automatically updates the pages to display in the browser.  You can do all editing in the web interface if you prefer, or edit the files directly.

Both vimwiki and Gollum allow me to organise my notes however I want and keeping them in a Git repo allows me to track and update my notes from all my devices.  Running Gollum on a webserver means I am also able to access my notes from any web browser, making it useful when I am travelling.

How do you track your notes and keep them accessible?  I am always interested in how others work in case it inspires changes to my own workflow.
