Most of us, if we realise it or not, have every day carry or EDC.  Before I had heard the phrase "every day carry" I had a couple of items that were always on my person.  Keys are an obvious daily carry, whether it is car keys or house keys you generally don't go outside without them.  That can be where it starts and ends, yet some of us decide to take the red pill and fall into a deep dark rabbit hole.

In recent years, since discovering a whole community of enthusiasts, my EDC has been updated.  I have introduced a few new items and swapped out old items for smaller versions making it easier to carry.

## core carry

### pouch contents
![pouch contents](/img/not_without_my_effects-pouch_contents.jpg#fitwidth)

* Abert Inox // wallet multi tool
* Olight // O'pen mini
* Rolling Square // inCharge USB cable
* Entroware // USB-C flash drive
* Sony // MDR-E9LP headphones
* Bank // credit card (not pictured)

These items have been my daily carry for many years.  Headphones are always handy when travelling on public transport, or when watching videos on the phone without disturbing others.  I generally carry cheap earbud headphones until they stop working then replace them.

I have always carried a pen of some description as well.  Back in the old days when I worked as a radar engineer and had to put pen to paper everyday, it was useful to have a couple of pens in my pocket.  I stopped carrying a pen on my person as often when I stopped having to wear shirts to work, as my Parker Jotter (see [extended carry](#extended-carry)) was a bit big for my pocket.  Since finding the Olight O'pen mini I am back to having a pen in my ~~pocket~~ pouch.

If you have followed my blog for a while you may remember my [a well-fortified position](20220830-a_well-fortified_position.html){target="_blank" rel="noreferrer"} entry, detailing my secure access setup for my servers and home VPN.  My disposable emergency SSH key lives on this USB-C flash drive.

The Abert Inox multi tool is a recent addition.  I found it in my father's camping gear and with it being able to fit nicely in my EDC pouch I started carrying it daily.

If I am carrying one of my larger EDC [pouches](#pouches) I will also have this mini flashlight on me.

![flashlight](/img/not_without_my_effects-flashlight.jpg#fitwidth)

* RovyVon // Aurora A3 Pro G4 MAO marble grey

This is another fairly new addition.  I had never thought about carrying a flashlight until discovering these mini versions.  In my previous career it was useful to have a flashlight in the van for when we were on the airfield working on equipment.  One of these keychain flashlights would have been very useful back then.

### pouches
![pouches](/img/not_without_my_effects-pouches.jpg#fitwidth)

* Garage Built Gear // mighty pouch+ black
* 52Graves // C.K.P. black
* Zerofeud // C.U.P. purple
* The Silver Express // charcoal pouch
* Zerofeud // black hex soft wallet
* Zerofeud // OD green hex soft wallet
* UYH.EDC // "33" pouch red

These days instead of just having my effects in my pockets I use an EDC pouch. The small 'soft wallet' pouches have been my longest serving but I have started to prefer the larger pouches for the organisational pockets and straps inside.

### pocket contents
![front pocket contents](/img/not_without_my_effects-key_organiser.jpg#fitwidth)

* OrbitKey // charcoal grey leather key organiser
* OrbitKey // USB 3.0 flash drive
* Trolkey // €1/€2 trolley token

As I said, carrying keys is probably everybody's first EDC.  I received this key organiser as a gift one Christmas and it has been in my pocket ever since.  Aside from my keys it holds a supermarket trolley token and a(nother!) flash drive.

![back pocket contents](/img/not_without_my_effects-hanks.jpg#fitwidth)

* InfiniteDesignsDE // zombie party hank
* InfiniteDesignsDE // Univers hank
* InfiniteDesignsDE // Hannibal hank
* InfiniteDesignsDE // Simpsons hank
* InfiniteDesignsDE // motherboard hank
* HinterlandEDC // pickle rick hank
* elefantihanks // purple pizza hank
* Three Dog Parcel // Tentacles hank
* Three Dog Parcel // Troopers hank

Since growing my beard in 2013 when I left the military, I have carried a hank.  From wiping away crumbs, or dabbing off beer foam, it is a much needed element of my EDC.  At first I carried cheap cotton hanks but over the last few years have picked up more interesting ones as you can see in the photo.  As well as cool patterns these hanks have cotton or microfiber backing, which is great for cleaning phone screens, glasses, or camera lenses.

### hydrate
![canteen](/img/not_without_my_effects-canteen.jpg#fitwidth)

* Bambaw // 1000ml stainless steel water bottle

Drink more water.  'nuff said.

## extended carry

So that is my core or 'actual' EDC.

I expand on this when carrying a bag.  My bag is not carried as much since working from home but when I do need it I like to have a few more effects on me.

![bag contents](/img/not_without_my_effects-bag_contents.jpg#fitwidth)

* Anker // PowerCore Essential 20000 powerbank
* Arteck // folding bluetooth keyboard
* Toshiba // 2TB external drive
* Ögon // Smart Case aluminum wallet
* Parker // Jotter pen
* Sharpie // black fine marker
* Friendly Swede // capacitive stylus
* Blue Spot // precision screwdriver
* Belkin // retractable Cat5e cable


I have been through many, many wallets over the years.  My favourite was a Ducti classic bifold.  My current wallet is hardy aluminum, which protects all my little RFIDs from space aliens.

Carrying a portable powerbank has become more of a necessity having not upgraded my phone in a few years.  This particular powerbank isn't as highly rated as some of Anker's other products but I have got a lot of use out of it and it is still doing okay.  I don't use my folding keyboard very often but it is small and light enough that it usually stays in my bag for when I do need it.  Same goes for the external drive, can you ever have too many data storage devices on you?

My Parker Jotter pen has been with me since I saw GoldenEye sometime in the late Nineties.  No joke, I got this after seeing the movie and it has been carried with me all through high school, college, my military service, and now it sits in my bag under my desk.  I still occasionally take it out to spin like Boris when I'm working on a difficult problem (whispering you know what when I succeed).  Having a Sharpie was more use when I was a field technician but old habits and all that.  The capacitive stylus I picked up for digital drawing, and while I rarely carry my tablet around I can always doodle on my phone.

Another old habit is carrying a screwdriver.  I don't have to take computers apart very often these days but this little driver is really handy.  The bit has two ends, one cross (#1) and one flat head (4.0mm).  It also comes with an array of other bits of different sizes and styles but I find this particular size to be the most useful.

Carrying a network cable is yet another throwback to the times I had to frequent datacentres, or in the times of LAN parties.  Still, it comes in useful often enough for it to stay in my bag.

### the bag
![messenger bag](/img/not_without_my_effects-bag.jpg#fitwidth)

* Diesel // messenger bag

In 2007 I saw Live Free or Die Hard, the forth movie in the Die Hard franchise.  It didn't get much love but I enjoyed it (and still do).  Justin Long's character has a messenger bag which he has modified for his hacking purposes.  It is a cool looking bag and for years I kept an eye out for it.  Finally, 15 years later I happened to see one (albeit a different colour) for sale and I couldn't resist.  Waiting for it to be delivered I started to worry it would be poor quality and not what I had hoped for all these years, but this is actually an excellent bag.

The quality is really good, with nice heavy buckles and chunky zips.  There are plenty of pockets and compartments, which is always a plus in my mind, and if Timothy Olyphant ever tries to start a fire sale I am one bag closer to stopping him.

## carry on
Being introduced to the EDC community has also inspired me to learn a bit more about photography.  Some people take amazing shots of their EDC, whether it's real or just for art.  If you follow me on the Fediverse you may have noticed that I have been posting some EDC photos.  As a complete photography noob I am very open to constructive criticism with the photos themselves or the editing, so don't be shy about letting me know what you think.

Who knows where my EDC will go next.  There are a few additional items I have my eye on, as well as potential replacements or alternatives, but this current carry has been doing me well for a while.  If you have any suggestions, or would like to give me your thoughts on my EDC then get in touch using the details on my [homepage](https://pyratebeard.net){target="_blank" rel="noreferrer"}.
