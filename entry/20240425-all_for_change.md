How many times do you use the `ls` and `cd` commands on GNU/Linux systems?  I imagine it is quite a lot.

I [briefly mentioned][1] my `chpwd_auto_cd` Zsh function, which will auto run the `ls` command when I `cd` into a directory.

Sometimes it is also useful to auto run `cd`.

A lot of the time when I create a new directory the first think I do is change into it, so I use this `mkcd` function to combine the two commands
```
mkcd() {
     mkdir -p "$1" && \
       cd "$1"
}
```

Another common use of `cd` is when I have cloned a Git repository.  So I have combined these into `gcl`
```
gcl() {
    git clone "${@}"
    test -n "${2}" && _dir=${2} || _dir=${1##*/}
    cd ${_dir%.git}
}
```

This function is a little more complex than `mkcd` as (remote) Git repositories have a URL which we don't want.  I use the special variable `$@` for the `git clone` command so it captures if I specify where to clone the repo, e.g.
```
git clone git://git.pyratebeard.net/dotfiles.git /tmp/dotfiles
```

If there is a second variable I set the `_dir` variable to it, otherwise I take everything after the final slash (`/`) of the URL.  For example, the repo URL _git://git.pyratebeard.net/dotfiles.git_ would give me a variable of `dotfiles.git`

Then I can change to the new directory, ignoring `.git` from the repo name if it exists.

[1]: 20231204-dot_matrix.html#epithet
