Recently I have been learning about photography.  It is something I started learning in my early teens but I didn't stick with it.

My wife has a [Nikon D3000](https://en.wikipedia.org/wiki/Nikon_D3000){target="_blank" rel="noreferrer"} which I started to practice with and have been trying to take photographs as much as possible.

We went on holiday recently, the perfect time to practice though I felt that the D3000 was a bit too bulky to take (with a small child luggage space was a premium - because of all their stuff, we didn't put the child in the luggage).  With that in mind I went looking for something smaller that still had all the manual control.  I found a [Panasonic Lumix LX3](https://en.wikipedia.org/wiki/Panasonic_Lumix_LX3){target="_blank" rel="noreferrer"} on a local marketplace website.  It is an older camera yet was affordable and after looking at some recent reviews felt like it was a good camera to take on holiday.  As soon as I picked it up I knew this was going to be a fun little camera.  It very quickly became part of my [EDC](20230809-not_without_my_effects.html){target="_blank" rel="noreferrer"}, being just about small enough to fit in my pocket.

In an effort to learn and improve my photography, armed with my trusty LX3, I will be attempting #photober and will try to take a photograph each day of October.  I have no idea what I will be shooting although I will try to be honest and take a new photo each day (instead of taking lots before hand and posting them each day).

If you would like to follow along I will be posting the photographs on the [Fediverse](https://harbour.cafe/@pyratebeard){target="_blank" rel="noreferrer"} and on my [Deviantart](https://www.deviantart.com/pyratebeard){target="_blank" rel="noreferrer"} profile.  The quality may not be very high as I am still learning both shooting and editing, nevertheless I do welcome constructive criticism.

<img src="/img/lx3.jpg#fitwidth" alt="Lumix LX3" />
