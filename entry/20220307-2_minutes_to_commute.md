I have been working in open plan office environments for over 10 years.  I have enjoyed some of my time in these environments and some of the time I have not.

Over the years I came to realise that the elements I did not enjoy stayed the same with each new job and each new office.  The noise.  The unsolicited interruptions.  Being dragged away from your desk into meetings that could have easily been an email, or more often than not should have easily been none of my concern, email or otherwise.

I don't treat a job as a place to make friends.  We are not "a family".  That is not to say I don't make friends at work, on the contrary; I have made some very close friends, but that's because we relate on a deeper level, not just a superfluous "we work together" level.

A few years ago I realised that the things I didn't enjoy about the office environments far outweighed the positives and so I started to look into working from home or remote working.  I knew it was something I wanted to try, but finding a job which allowed remote working was not an easy task, in fact I was unable to succeed in this endeavour.

As a contractor I have to rely on recruitment agencies to find positions, very few companies hire direct.  I would always stipulate my desire to work remote but the answer was usually the same, "some work from home may be allowed at the employers discretion".

I would ask about remote working potential in the interview, unsurprised when I was told it wasn't really done at [_insert company name_].

So I continued to commute.  My commutes have varied, but having lived and worked in two major cities the majority of my commutes have been an hour on public transport.  I would try to use that time to read or catch up on podcasts.  The downsides are when there was any transport issues it had a knock on effect and having to be prepared for all weather.

Then in early 2019 the whole world found out what working from home was really like.

I had been at my job for two weeks when we got told to pack up and head home.  I can't speak for every company but the one I work for did an amazing job at getting people set up at home with equipment, and allowing some slack on getting use to the new way of working.  We adapted fairly quickly though and I for one settled into my new environment, enjoying it while I could.

Two years have gone by since I have stepped foot in an office and long may it continue.  In last year I also became a father, which has given me a whole new love for being at home all day.

When I first started to work from home I tried to follow all the guidelines I had read.  Separate your "work" and "home" by being able to close a door if required.  Get dressed so you are "ready for work".  Stick to normal office hours.

This all worked well and got me into to a good routine.  I even tried taking half an hour at the end of my work day to listen to a podcast or read, this meant I wasn't completely losing my commute activity.

Some of these guidelines have relaxed a little, especially since having a child in the house.  I stick to normal office hours and I still have a separate workspace with a door.  I'm a bit more relaxed about putting on jeans in the morning though, a couple of hours in lounge pants really gets the mind focused.

I was always an advocate for remote working, now more than ever I think companies need to adapt.  It needs to become normalised.  We have proven it is possible.  We have seen the benefits of not commuting.

That is not to say we should do away with offices.  Many people prefer the office environment, and that is fine, but everybody should be allowed a choice.  If you want to work from home so you can be around your family more, do it.  If you prefer to work from home so you don't get disturbed, or you want quiet, then do it.

A lot of companies are going to try and go back to the way things were but I think it will be a losing battle.  Everybody has tasted working from home and for those that prefer it, companies should accommodate them.
