Great news in France, the CNIL (Commission Nationale de l'Informatique et des Libertés) is ordering Google and Facebook to make declining cookies as easy as accepting them.  The full story can be found on [arstechnica.com](https://arstechnica.com/tech-policy/2022/01/frances-fines-google-and-facebook-e210m-for-making-cookies-hard-to-reject/).

I hate cookie pop-ups, especially on mobile devices.  They are everywhere and they are a pain in the ass.  Having a simple "accept all" button while making users jump through hoops to disable all but the absolutely necessary cookies, is sly and distasteful.  On mobile devices the pop-up generally completely covers the webpage so you can't ignore it.  On more than one occasion I have navigated back away from the site, I probably didn't need to see that cat picture anyway.

The original use for cookies was for e-commerce, to track your shopping cart between pages.  Another use is for secure login to sites.  It would be extremely annoying if you couldn't log into a website account but instead entered your password on every page of restricted data.  Session management is a fairly acceptable use of cookies.

In my opinion anything above and beyond the absolutely necessary should be opt-in, not opt-out.  Every website you visit should not hide itself behind a disgraceful pop-up but instead have somewhere on the page the option to turn on personalisation and tracking cookies.  Most users who say they don't mind getting personalised ads probably wouldn't go hunting for the option to turn them on, they do not care enough either way.  Those who do still have the option.  This would make the world wide web a much nicer place to visit, where you can browse webpages at leisure without first disabling all the gumph moneymaking advertisers are forcing on us.  In fact, none of this would be required if websites took your browser's "Do Not Track" option as the response to the big cookie question.

The world wide web use to be full of interesting things and endless wonder, not the paywall, ad filled, cookie pop-up cesspool it is today.

/rant

It isn't all doom and gloom though, there are some parts of the internet which are still pleasant.  The [Gopher](https://mncomputinghistory.com/gopher-protocol/) protocol seems to be having a resurgence and the [Gemini](https://gemini.circumlunar.space/) protocol is filling in the gap for those who want a bit more from Gopher but still want to steer clear of what HTTP is being used for.  I wrote previously about my first foray [down the gopherhole](https://log.pyratebeard.net/entry/20210302-down_the_gopherhole.html) and I have been trying to use it a lot more.  It is definitely an interesting space.

There are also projects like the [512kb Club](https://512kb.club/) who encourage minimalistic websites, sites under 512kb uncompressed (this site is currently only [63.8kb](https://gtmetrix.com/reports/log.pyratebeard.net/xLugqn2j/#waterfall) uncompressed).

The internet isn't a lost cause if there is but one fool left to fight for it.  Here's hoping the French can begin a snowball effect to end the dreaded cookie pop-up.  Else we see more people spending their time on Gopher, Gemini, or anywhere else still with the love of content creation, where people's curiosity is rewarded and they can expand their minds instead of doomscrolling until bedtime.
