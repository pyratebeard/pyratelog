In my [post about SSH keys with GPG](20221202-secret_agent_man.html){target="_blank" rel="noreferrer"} I mentioned that I had put the following in my `zsh` config
```
gpg-connect-agent updatestartuptty /bye >/dev/null
```

This was because GPG has no way of knowing which TTY you are using for displaying `pinentry`.

I have updated my configuration now as that setup was causing some issues.

As my `zsh` config would run on every new terminal I started, GPG would always display `pinentry` on the most recent terminal, not necessarily on the terminal I was using.

To fix this I removed that command from my config and instead included the following in ~/.ssh/config
```
Host localhost
  ForwardAgent yes
  AddKeysToAgent ask
  Match host * exec "gpg-connect-agent updatestartuptty /bye"
```

Now `gpg-connect-agent` will run when I initiate `ssh` and update the TTY to whichever one I am using.  Thanks to [this answer](https://unix.stackexchange.com/a/587691){target="_blank" rel="noreferrer"} on Stack Exchange for the fix.

The reason I needed this fix was because `pinentry` would pop up quite regularly when running `ssh`.  I had the `default-cache-ttl` and `max-cache-ttl` values in ~/.gnupg/gpg-agent.conf so that GPG would cache my passphrase for an entire day but that didn't appear to be applied to SSH.  I read through the man pages didn't find anything to help.  I am sure I am missing something, hopefully in the next week or two I can figure it out.

In the meantime, my updated setup is much smoother now that I don't have to go hunting for `pinentry`.
