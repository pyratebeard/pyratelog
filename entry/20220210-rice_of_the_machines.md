The first Desktop Environment (DE) I ever used on a Linux distro was KDE (circa 2008).  Coming from Windows I was amazed at how much you could customise the theme and style of the windows, even back then I preferred dark mode everything.  I started to play around with themes and colours, and this customising continued through my use of Gnome 2 and then XFCE.  By this point I had started to notice screenshots of Linux desktops online, leading me eventually to [/r/unixporn](https://reddit.com/r/unixporn) and the term "ricing".

According to the [/r/unixporn wiki](https://www.reddit.com/r/unixporn/wiki/themeing/dictionary/#wiki_rice)

> "Rice" is a word that is commonly used to refer to making visual improvements and customizations on one's desktop. It was inherited from the practice of customizing cheap Asian import cars to make them appear to be faster than they actually were - which was also known as "ricing"

I was jubilant to find a whole community of people who enjoyed customising their systems.  Through /r/unixporn I was introduced to new techniques, tools, software, and other communities.

My ricing became more deliberate.  I started to see ricing as an art form, taking inspiration from others, pictures, or even single colours.  I moved away from DEs and began using a Window Manager (WM).  I also paid more attention to the software I used and tried to customise everything I could.  I also began to [share my ricing screenshots](https://www.reddit.com/r/unixporn/search?q=author%3Apyratebeard).

A few /r/unixporn ricers (xero, venam, z3bra) lead me to the [nixers](https://nixers.net) community.  While not strictly a "ricing" community there are a number of members who rice.  Recently it was decided to showcase some of [our screenshots](https://screenshots.nixers.net).

I see a lot of posts on /r/unixporn and [/r/unixart](https://reddit.com/r/unixart) with ricers saying "this is it, my finished rice", and I always chuckled at this.  I was always playing around with my colours and styles, forever tweaking.  There is no end... I thought.

I haven't made any changes to my current theme in a long time.  It was a lot of tweaking to get it how I like it, but the style, colours, usability, have all worked so well that I haven't needed to adjust it.  I like this rice so much I entered it into the August 2021 /r/unixporn ricing competition and took [second place](https://www.reddit.com/r/unixporn/comments/pnkbev/august_ricing_competition_winners/) (thanks to all who voted!).

This isn't to say I have finished ricing, I have a few ideas for new projects, but have been so happy with what I have now that maybe "this is it, my finished rice".  Somebody will probably chuckle at the naivety of that statement.

If you want to get into ricing check out /r/unixporn and /r/unixart to get an idea of what is possible.  [Dotshare](http://dotshare.it) is a great source for themes and their accompanying configuration files.  I found [gpick](https://www.gpick.org) a great tool for finding colours and whole palettes from images.  The website [terminal.sexy](https://terminal.sexy) is also useful to play around with the colours and see how they look together.

Take your inspiration from anything.  I have riced based on comic books, album covers, and even a single colour.

Some of my favourite rice screenshots by others are:

* [bbs lyfe](https://i.redd.it/vi7crebm52sx.png) by xero
* [hyper light drifter](https://i.redd.it/yqgwrh1ojebz.png) by novcn
* [creation](https://i.redd.it/t8p0l43zjvm01.png) by szorfein
* [muspelheim](https://i.redd.it/n5m7rap66ii71.png) by barbarossa
* [gvfr](https://u.teknik.io/qc8dI.png) by nxll

A collection of my rice screenshots can be [found here](https://pyratebeard.net/scrots.html).
