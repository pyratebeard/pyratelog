Fonts in the terminal are mostly personal preference.  I have been using the same font for many years, [Tamzen font by Suraj N. Kurapati](https://github.com/sunaku/tamzen-font){target="_blank" rel="noreferrer"}, which is a fork of the font I before that, [Tamsyn font by Scott Fial](http://www.fial.com/~scott/tamsyn-font/){target="_blank" rel="noreferrer"}.

Even after trying others over the years I always came back to Tamzen.  One of my IRC buddies had created a couple of fonts, one of them I have been using a bit this week, [viscera](https://gitlab.com/Barbaross/Muspelheim/-/tree/xorg/.local/share/fonts){target="_blank" rel="noreferrer"}.  Feeling inspired I installed [gbdfed](http://sofia.nmsu.edu/~mleisher/Software/gbdfed/){target="_blank" rel="noreferrer"} and started playing around.

The font style started to look eldritch, so I lent into that and ended up with what I have named [trigon](https://pyratebeard.net/trigon-font/){target="_blank" rel="noreferrer"}.

![fontpreview](/img/trigon-preview.png#fitwidth)

As this is my first font there is a limited character set, and only one size.  You can download the [BDF file](https://pyratebeard.net/trigon-font/trigon8x15r.bdf) to try the font out yourself.

Any suggestions or comments are welcome.  You can find contact information on my [home page](https://pyratebeard.net).
