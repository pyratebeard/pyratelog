It has been a [little over a month](20220113-colemak_exp.html) since I switched to using the Colemak keyboard layout and I am quite happy with how comfortable I have become with it.  The first couple of weeks were tough.  I found the extra concentration needed was quite draining and the frustratingly slow typing didn't help.

I have used it every weekday for work, only using QWERTY on my phone and a couple of times on my laptop.  By the end of each week I was finding it easier, then after barely using the keyboard over the weekend the first couple of days back were slow.  That time to speed up is improving though.

The main issue I thought I would have was not as problematic as expected, however there have been issues I didn't foresee.

## ghost of the navigation
After the first couple of days I was expecting my heavy reliance on Vim navigation to be a hindrance, now that the H, J, K, L keys are not situated together on the home row.  As you may recall I installed `xte` and set `xbindkeys` keybindings to emulate J and K by pressing Alt+n and Alt+e respectively.  I actually haven't used these keybindings at all and have very quickly become use to the positioning of H, J, K, L.  That being said, my navigation within Vim has changed.  Probably for the better.  Due to the position of the basic navigation keys I stopped relying on them as much and started using alternative navigation (`w`, `e`, `b`, `f`, etc.) and count navigation a lot more.  I also started learning the jump list, something I hadn't really touched on before.

Vim navigation in other tools still use H, J, K, L, but I have grown accustomed to it very quickly, so that hasn't been the blocker I thought it would be.

## fear of the boot
I don't reboot my PC very often so I didn't consider requiring Colemak at boot.  I shut everything down the other day while I was [painting my wall](https://mastodon.social/@pyratebeard/107751942807459892){target="_blank" rel="noreferrer"}, so when I booted up I had to be really careful typing in my encryption passphrase and login password (looking at the QWERTY keyboard on my phone while I typed).  I should really try and fix that before I next reboot.

## the vm factor
I run virtual machines on my PC for a multitude of reasons but not all of these have Colemak immediately available, for example the Debian installer doesn't list Colemak (but it does have Dvorak).  I can switch my system keymap to QWERTY to make things easier but I find it hard to switch back and forth so quickly.  I think it is made worse by the physical keys being in a different layout, if I switch to my laptop (with a QWERTY keyboard) I don't have any difficulty typing.

This is only really an issue when I am installing a clean distro.  Most of the time I configure a base VM to clone, so I can make sure Colemak is working.  This won't help me in work though, unfortunately accessing VMs through the web console doesn't pass through the system keymap.  All of this has meant some "quick and easy" jobs took a bit longer.

## brave new world
After a frustrating start I have actually started to enjoy the Colemak layout.  My typing is still not perfect but I have seen a huge improvement in the last week.  For now I will stick with it, dealing with the VM issue as and when.  If anybody has any good suggestions to dealing with that let me know.

I am also going to practice switching back and forth between Colemak and QWERTY, in the hopes that I can change seamlessly on the same keyboard.  If that goes anywhere I may write another entry about it.
