If you have used `sed` then you have used the `/` delimiter
```
sed s/foo/bar/g file
```

You will also probably be aware the need to escape any slash in the string you're trying to match, for example matching `f/o/o` would be
```
sed s/f\/o\/o/b\/a\/r/g file
```

What you may not know is you can use different delimiters to save yourself escaping any occurrence of slash in the string.

```
sed s%foo%bar%g file
sed 's|foo|bar|g' file
sed 's foo bar g' file
```

Note that some delimiters require quotes.  Any occurrence of your chosen delimiter in the string will have to be escaped.

According to section 4.3 of the `sed` Info document you should be able to use "any single character", giving you a lot of options to chose from in order to avoid all the escaping.  To read this section of the Info document incant
```
info sed -n "regexp addresses"
```

Do you have a preferred delimiter or do you like a bit of variety?
