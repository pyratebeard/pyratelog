Today I was introduced to the concept of a "/now" page.  Most of us are familiar with the /about page on personal websites, which provides an overview or background of the person.  The idea behind a /now page is to provide more current information or "what this person is focused on at this point in their life".

The idea began with a blog post by [Derek Sivers](https://sive.rs/nowff){target="_blank" rel="noreferrer"}.  The post caught traction leading to the creation of [nownownow.com](https://nownownow.com){target="_blank" rel="noreferrer"}, a site that lists people who have included a /now page on their own websites.

Inspired by this I have created a [Current focus](https://pyratebeard.net/#now){target="_blank" rel="noreferrer"} section to my homepage, listing things like my latest log entry and photograph, and even the coffee I am currently brewing.

In true nerd fashion the information is also available in my [finger information](20230518-pull_my_finger.html){target="_blank" rel="noreferrer"}
```
finger pyratebeard@pyratebeard.net
```

I will endeavour to keep this information as current as possible.  At the moment I am planning to update it at least weekly.  There are a few ideas I have in mind of how best to keep the section updated, we shall see how it goes.
