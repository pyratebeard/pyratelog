Have you ever created a directory in a long file path, then have to type out the long file path again with `cd`?  Well no more!
```
mkdir /a/super/long/file/path/to/my/new/directory/
cd !$
```

_Note: does not work with all shells. I know bash and zsh work._ 

In some shells the exclamation mark (!) can be used as an event designator to reference the command history.  A lot of users have probably come across this for running the last command again, maybe if they forgot to use `sudo`
```
$ pacman -Syu
error: you cannot perform this operation unless you are root.
$ sudo !!
```

It can also be used with a number to run that command from the history, for example to run the very first command in your history incant
```
!1
```

Something I use quite a lot is the ability to reference a field from the last command, mostly the last field (`$`).  As shown above you can use it to navigate to a directory you have just created, or anything requiring the last field of the previous command.

You can also reference the first argument using `!^` or the command word using `!:0`.  Note the colon before the zero.

These event and word designators are quite extensive so check the manpages for more information.
