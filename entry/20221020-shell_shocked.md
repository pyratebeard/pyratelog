I didn't know that [vim](TK){target="_blank" rel="noreferrer"} could open a terminal in a window split.

Usually I have vim in a [tmux](TK){target="_blank" rel="noreferrer"} session so can split the tmux window to get a terminal.  Occasionally I would use `:shell`, which opens a shell in the current terminal but backgrounds vim so you have to exit the shell to return.  This is useful but it can be beneficial to keep your buffer open while running a command, and vim may not be in a tmux session.

As of vim 8 the terminal feature is natively supported.  Neovim also has this feature natively.  To see if your vim has this feature incant the following
```
:echo has('terminal')
```

If the output is `1` then you have it.

To simply open a terminal in a split incant
```
:terminal
```
or
```
:term
```

You can append a command, which once completed will open the output in a buffer ready for editing.

This is a cool feature, and I will probably use it quite a bit.  It is great fun to be constantly learning vim, even finding features that have been around for a while.

For more information on the terminal feature check out the help page
```
:help terminal
```
