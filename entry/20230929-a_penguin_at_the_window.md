I have been daily driving Linux since 2008 when I settled on Fedora 9.  In that time I have adapted my workflow to suit me, one of the beautiful things of using Linux over Windows or Mac.

My current workflow is so unique, with custom keybindings, scripts, and configurations; that few people could probably use it comfortably.  I am sure this is true for many Linux users of 15 plus years.

I am also a professional Linux systems consultant, yet in every 'Linux' job over the last 10 years I have been made to use a Windows system to carry out my work.

In only two jobs have I been allowed to run a Linux VM on my Windows machine.  I really struggle to use Windows to work.  For those that have seen my [ricing](https://pyratebeard.net/scrots/){target="_blank" rel="noreferrer"} will know that I do _almost_ everything via the terminal.  I prefer to use a keyboard over a mouse, something that is very difficult in a GUI focused Windows system.  Even when I do have to use a GUI I prefer it to be unobtrusive.  Trying to view a spreadsheet in Excel is horrendous when the top half of the screen is taken up by huge menu buttons.  I know these menus can be compacted, but they still take up far too much space in my opinion.

I have little choice though, so I succumb to using the tools available to me.  PuTTY is decent, and allows a lot of configuration but I find configuring it to be slow and clunky.  More recently I have tried to use WSL where possible.  At least then I can make use of ssh configuration files.  Not all jobs I have worked in have allowed WSL, or if they have the connection to the repos doesn't work so I am left with only what is installed by default (and no updates!).

The worst 'workflow' I have experienced was using a Windows laptop, I would connect to a virtual Windows desktop in the browser(!!), then RDP to a Windows server in the environment, ssh using PowerShell (no WSL or PuTTY) to a jump server where I had to ssh to another Linux server which had Ansible and the other tools needed to do my work.  Any latency in that ridiculous connection made working unbearable.

The subject of this log entry comes from having recently started a new job and yet again faced with trying to find a comfortable workflow on Windows.  Something that has been made even more difficult now that I have switched to [Colemak](20220825-colemak_+2.html){target="_blank" rel="noreferrer"} as Windows doesn't ship that keyboard layout.

I don't like to rant about this (much), I am lucky to have a career I enjoy and all my colleagues over the last decade have managed to work.  So why does it aggravate me so much?  Why, when I'm told I can't use Linux for security reasons, do I get so vexed.

When interviewing for new jobs I always ask the same few questions, one of those being "Will I be allowed to run a Linux system to carry out my job?".  I have never heard the answer "Yes".

There is no real point to this entry, it is more an outlet for me to get this off my chest.  No doubt I will go through the same irritation in my next job, and every job hence.

Let me know if you have managed to find a comfortable workflow in Windows; or if you work in a job that allows Linux, are you hiring? :)  Contact details can be found on my [homepage](https://pyratebeard.net){target="_blank" rel="noreferrer"}.
