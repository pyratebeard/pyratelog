I use Ansible quite a lot these days, and I find the coloured output useful when I am skimming through the results.

The other day I had to capture the output as a file to show a colleague, so I used `tee`
```
ansible-playbook playbook.yml | tee results.out
```

By piping to `tee` Ansible doesn't show any colour.  This is common if a command doesn't think it is writing to an interactive output.

A quick internet search provided a [useful solution](https://superuser.com/a/751809){target="_blank" rel="noreferrer"}, to use the `unbuffer` command.  You can get this command by installing the `expect` package on Arch or rpm-based distros or `expect-dev` on deb-based distros.

Now prepend `unbuffer` to your command
```
unbuffer ansible-playbook playbook.yml | tee results.out
```

This will also add the colour codes to your output file so if you use `less` add the `-R` option
```
less -R results.out
```

This will work with any command, not just `ansible-playbook`.

A downside to `unbuffer` is that it doesn't recognise aliases.  I alias `ansible-playbook` to `ap` but cannot use that with `unbuffer`.  I have seen an alternative way of preserving colour using the `script` command but it was not as easy to get working.

It is not often that I need this so I will stick with `unbuffer` for now.  If you know of any other methods I would be happy to hear them, contact details can be found on my [homepage](https://pyratebeard.net).
