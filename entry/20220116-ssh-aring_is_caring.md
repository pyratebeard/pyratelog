Have you ever logged on to a server over SSH and had to type in your really long and complicated password (let us assume for some reason you haven't configured SSH keys and a key agent), then wanted to open a second connection to that same server only to type your long and complicated password again?

What a pain in the ass.

Did you know that you can 'share' SSH sessions?

By using the `ControlMaster` and `ControlPath` config options you can open a session to a server using your long and complicated password, then every subsequent session to the same server shares the connection... no more password typing.

In your ~/.ssh/config add the following
```
Host *
  ControlMaster auto
  ControlPath ~/.ssh/%r@%h:%p
```

* `%r` - remote username
* `%h` - remote hostname
* `%p` - remote port

Further information on these and other options can be found in the ssh_config(5) man page.

This session sharing will work until the initial connection is closed, after which all subsequent connections will close, so be careful.

If you would rather the initial connection remain open in the background you can add the following to ~/.ssh/config
```
  ControlPersist yes
```

You will have to kill or close that connection using an alternative method, e.g. `ssh -O exit`.

Read the ssh_config(5) man page for further information.
