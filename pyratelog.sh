#!/bin/bash

WEBDIR="/var/www/html"

cd ${WEBDIR}

# generate rss.xml file
if [ -f rss.xml ] ; then
	if [[ $(head -n 1 rss.xml) == "<?xml version='1.0' encoding='UTF-8'?>" ]] ; then
		tail -n+7 rss.xml > rss.xml.bak
		mv rss.xml.bak rss.xml
	fi
else
	echo -e '</channel>\n</rss>' > rss.xml
fi

# find all markdown files in 'entry' directory
find_md=$(find entry/ -type f -name "*.md" | sort)

# for each markdown file
for md in ${find_md} ; do

	# get the title and date from the filename
	input=$(echo ${md} | cut -f2 -d '/' | rev | cut -f2- -d '.' | rev)

	# cut the date and turn into epoch time
	input_date=$(echo ${input} | cut -f1 -d '-' )
	if ls -l /bin/date | grep busybox >/dev/null ; then
		# busybox `date` works differently
		index_date=$(date -d ${input_date} -D "%Y%m%d" +%s)
		rss_pubdate="<pubDate>$(date -d ${input_date} -D '%a, %d %b %Y 00:00:00' +%s)</pubDate>"
	else
		index_date=$(date -d ${input_date} +%s)
		rss_pubdate="<pubDate>$(date -d ${input_date} +'%a, %d %b %Y 00:00:00')</pubDate>"
	fi

	# cut the title and replace underscores with spaces for menu
	input_title=$(echo ${input} | cut -f2- -d '-' | sed 's/_/\ /g')
	index_title=$(echo ${input_title})

	# create menu link for index page
	input_link="<li><a class='index' href='entry/${input}.html'><b>${index_date}</b> ${index_title}</a></li>"

	# if link already exists we can skip
	grep ${input} index.html >/dev/null && continue

	# for new files we turn the markdown into html
	# using a template file
	pandoc -s \
		--template=./entry_template.html \
		--metadata title="${input_title}" \
		-f markdown \
		-t html \
		-o entry/${input}.html \
		entry/${input}.md
	
	# add the link to the top of the menu
	sed "/<ul class='index'>/a ${input_link}" index.html > index.html.bak

	# replace old index page
	cat index.html.bak > index.html

	# tidy up
	rm -f index.html.bak

	# create rss item
	rss_title="<title>${index_title}</title>"
	rss_link="<link>https://log.pyratebeard.net/entry/${input}.html</link>"
	sed "s/</\&lt\;/g; s/>/\&gt\;/g" entry/${input}.html > entry/${input}.html.rss
	rss_description=$(sed -n '/body/,/\/body/p' entry/${input}.html.rss)

	echo -e "\t<item>\n\t\t${rss_title}\n\t\t${rss_pubdate}\n\t\t${rss_link}\n\t\t<description>${rss_description}</description>\n\t</item>\n$(cat rss.xml)" > rss.xml

	rm -f entry/${input}.html.rss
done

if [[ $(head -n 1 rss.xml) != "<?xml version='1.0' encoding='UTF-8'?>" ]] ; then
	echo -e "<?xml version='1.0' encoding='UTF-8'?>\n<rss version='2.0'>\n<channel>\n\t<title>pyratelog</title>\n\t<link>https://log.pyratebeard.net</link>\n\t<description>another personal blog by another geek</description>\n$(cat rss.xml)" > rss.xml
fi
