#TITLE := $(shell git branch --show-current)
RND := ""
# https://stackoverflow.com/a/14061796
# If the first argument is "draft"
ifeq (draft,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "draft"
  ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(ARGS):;@:)
else
ifeq (preview,$(firstword $(MAKECMDGOALS)))
  ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(ARGS):;@:)
else
ifeq (publish,$(firstword $(MAKECMDGOALS)))
  ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(ARGS):;@:)
endif
endif
endif

ifeq ($(ARGS), )
	ARGS := $(shell git branch --show-current)
endif

prog: # ...
    # ...

.PHONY: draft preview publish

draft : prog
	scripts/draft $(ARGS) $(RND)

preview : prog
	scripts/preview $(ARGS)

publish : prog
	scripts/publish $(ARGS) $(RND)
